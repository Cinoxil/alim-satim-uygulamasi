[![version](https://img.shields.io/static/v1?label=Version&message=1.0.2&color=blue)](https://gitlab.com/Cinoxil/alim-satim-uygulamasi/-/tags/v1.0.2)
[![Licance](https://img.shields.io/static/v1?label=Licance&message=MIT&color=brightgreen)](https://gitlab.com/Cinoxil/alim-satim-uygulamasi/-/blob/master/LICENSE)


# Alim-Satim Uygulamsi

<!-- Icindekiler -->
<details open="open">
  <summary>Icindekiler</summary>
  <ol>
    <li><a href="#aciklama">Aciklama</a></li>
    <li>
      <a href="#baslarken">Baslarken</a>
      <ul>
        <li><a href="#geresinimler">Geresinimler</a></li>
        <li><a href="#yukleme">Yukleme</a></li>
      </ul>
    </li>
    <li><a href="#kullanim">Kullanim</a></li>
    <li><a href="#lisans">Lisans</a></li>
    <li><a href="#iletisim">Iletisim</a></li>
  </ol>
</details>    

Alim-Satim Uygulamsi urun ve para ekleyip, eklenen urunlerin ticaretini yapabliceginiz bir uygulamadir.

## Aciklama
Uyulama Visual Studio 2019 ile gelistirilmistir. Linq, Newtonsoft JSON teknolojilerinden yararlanarak gelistirilmis basit bir p2p alisveris uygulamasidir.

## Baslarken

### Geresinimler

Visual Studio 2019 ile hazirlanmistir ve Visual Studio 2019'u indirip kullanabilirsiniz. Visual Studio 2019 indirmek icin: [[Visual Studio 2019]](https://visualstudio.microsoft.com/tr/downloads/)

### Yukleme
* Depoyu klonlayin
   ```sh
   git clone https://gitlab.com/Cinoxil/alim-satim-uygulamasi.git
   ```

## Kullanim
Admin bölümü için admin hesabiyla giris yapmak gerekmektedir. Admin hesabi:
```python
Kullanicı adi: admin
Sifre: 123
```
Uygulamayi Visual Studio'da kullanirken cikis islemini stop debug ile değil formu kapatan carpi isaretinden yapiniz, yoksa bilgileriniz kayit olmaz.

## Lisans
[MIT](https://gitlab.com/Cinoxil/alim-satim-uygulamasi/-/blob/master/LICENSE)

## Iletisim
* [![Gmail](https://img.shields.io/badge/-Okan_Erciyas-c14438?style=flat&logo=Gmail&logoColor=white)](mailto:okanerciyas8@gmail.com)
[![Linkedin](https://img.shields.io/badge/-Okan_Erciyas-blue?style=flat&logo=Linkedin&logoColor=white)](https://www.linkedin.com/in/okan-erciyas-006959192/)

* [![Gmail](https://img.shields.io/badge/-Cüneyt_Uğur_Öngün-c14438?style=flat&logo=Gmail&logoColor=white)](mailto:c.ugurongun@gmail.com)
[![Linkedin](https://img.shields.io/badge/-Cüneyt_Uğur_Öngün-blue?style=flat&logo=Linkedin&logoColor=white)](https://www.linkedin.com/in/cüneyt-uğur-öngün-650162131/)
