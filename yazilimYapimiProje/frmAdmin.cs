﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using yazilimYapimiProje.Entities;
using yazilimYapimiProje.PanelIslem;

namespace yazilimYapimiProje
{
    public partial class frmAdmin : Form
    {
        public frmAdmin()
        {
            InitializeComponent();
            dgPara.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgUrun.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //Form closing event olduğunda adminFormKapatma fonksiyonunu çalıştırır.
            FormClosing += adminFormKapatma;
        }

        /*Bu fonksiyon eğer kullanıcı admin formundayken çarpı işareti ile kaptırsa verilerin kaybolmamsı için ve programın doğru bir biçimde
          kapanması için yazılmıştır.*/
        private void adminFormKapatma(object sender, FormClosingEventArgs e)
        {
            //Açık formlardan adı frmAna olanı yakalıyor.
            var anaForm = Application.OpenForms["frmAna"];
            //Anaformu kapatıyor.
            if (anaForm != null) anaForm.Close();
        }

        private void btnOturumKapat_Click(object sender, EventArgs e)
        {
            panelIslemleri.oturumuKapat();
        }

        private void frmAdmin_Load(object sender, EventArgs e)
        {
            #region Para istekleri

            //Para istekleri datagridwiewının sütun sayısını oluşturup sütun isimlerini atıyor.
            dgPara.AllowUserToDeleteRows = false;
            dgPara.ColumnCount = 4;
            dgPara.Columns[0].Name = "Para İstek Id";
            dgPara.Columns[1].Name = "Id";
            dgPara.Columns[2].Name = "Miktar";
            dgPara.Columns[3].Name = "Para Birimi";
            //paraIstekleriListele fonksiyonuna dgPara datagridwiewinin yollayıp listeleme işlemini yapıyor.
            paraIstekleriListele(dgPara);

            #endregion

            #region Ürün istekleri

            //Ürün istekleri datagridwiewının sütun sayısını oluşturup sütun isimlerini atıyor.
            dgUrun.AllowUserToDeleteRows = false;
            dgUrun.ColumnCount = 5;
            dgUrun.Columns[0].Name = "Urun Id";
            dgUrun.Columns[1].Name = "Id";
            dgUrun.Columns[2].Name = "Ürün Adı";
            dgUrun.Columns[3].Name = "Miktar";
            dgUrun.Columns[4].Name = "Fiyat";
            //urunIstekleriListele fonksiyonuna dgPara datagridwiewinin yollayıp listeleme işlemini yapıyor.
            urunIstekleriListele(dgUrun);

            #endregion

            #region doviz label

            double[] dovizler = new double[3];
            dovizler = (double[])paraIstek.dovizIslem(); //dovizler arrayine paraIstek.dovizIslem'den gelen degeri cekiyoruz

            lbl_usd.Text = dovizler[0].ToString(); //burada ilgili labellara esitliyoruz
            lbl_eur.Text = dovizler[1].ToString();
            lbl_gbp.Text = dovizler[2].ToString();

            #endregion

            #region bakiye label

            double adminBakiye = dataBase.dataBase.adminler.Where(x => x.id == "admin").FirstOrDefault().komisyonPara; //admini bulup komisyonParadaki degeri adminBakiyeye esitliyor
            adminBakiye = Math.Round(adminBakiye, 2); //adminBakiyeyi virgulden sonra 2 hane round ettik
            lblBakiye.Text = adminBakiye.ToString(); //ilgili labela adminBakiyeyi yazdiriyoruz


            #endregion
        }

        #region dgPara cellClick işlemleri

        private string
            kullaniciId; //Değişken dgPara_CellClick ve dgUrun_CellClick te kullanıldığı için global tanımlandı.

        private int
            paraIstekId; //Değişken cellclick, onayla ve reddet işlemlerinde kullanıldığı için global tanımlandı.
        private string paraBirimi;

        private void dgPara_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > -1) dgPara.Rows[e.RowIndex].Selected = true;
            //kullanıcıIdye bir numaralı indexdeki idyi atıyor.
            kullaniciId = dgPara.Rows[e.RowIndex].Cells[1].Value.ToString();
            //paraIstekIdye sıfır numaralı indexdeki idyi atıyor.
            paraIstekId = Convert.ToInt32(dgPara.Rows[e.RowIndex].Cells[0].Value);
            paraBirimi = (dgPara.Rows[e.RowIndex].Cells[3]).Value.ToString();

            lblPara.Text = "Para İstek Id : " + dgPara.Rows[e.RowIndex].Cells[0].Value
                                              + " Id : " + dgPara.Rows[e.RowIndex].Cells[1].Value
                                              + " Miktar : " + dgPara.Rows[e.RowIndex].Cells[2].Value
                                              + " Para Birimi : " + dgPara.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        //Bu fonksiyon ile seçilen satırın arka plan rengini kırmızı, yazı rengini beyaz yapıyor.
        private void dgPara_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //Eğer bir satır seçilmişse içeri giriyor.
            if (dgPara.Rows[e.RowIndex].Selected)
            {
                e.CellStyle.SelectionBackColor = Color.Red;
                e.CellStyle.SelectionForeColor = Color.White;
            }
        }

        #endregion

        #region dgUrun cellClick İşlemleri

        private int urunId; //Değişken cellclick, onayla ve reddet işlemlerinde kullanıldığı için global tanımlandı.

        private void dgUrun_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e != null && e.RowIndex > -1 && e.ColumnIndex > -1) dgUrun.Rows[e.RowIndex].Selected = true;
            //kullanıcıIdye bir numaralı indexdeki idyi atıyor.
            kullaniciId = dgUrun.Rows[e.RowIndex].Cells[1].Value.ToString();
            //urunIdye sıfır numaralı indexdeki idyi atıyor.
            urunId = Convert.ToInt32(dgUrun.Rows[e.RowIndex].Cells[0].Value);

            lblUrun.Text = "Urun Id : " + dgUrun.Rows[e.RowIndex].Cells[0].Value
                                        + " Id : " + dgUrun.Rows[e.RowIndex].Cells[1].Value
                                        + " Ürün adı : " + dgUrun.Rows[e.RowIndex].Cells[2].Value
                                        + " Miktar : " + dgUrun.Rows[e.RowIndex].Cells[3].Value
                                        + " Birim fiyat : " + dgUrun.Rows[e.RowIndex].Cells[4].Value;
        }

        //Bu fonksiyon ile seçilen satırın arka plan rengini kırmızı, yazı rengini beyaz yapıyor.
        private void dgUrun_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //Eğer bir satır seçilmişse içeri giriyor.
            if (dgUrun.Rows[e.RowIndex].Selected)
            {
                e.CellStyle.SelectionBackColor = Color.Red;
                e.CellStyle.SelectionForeColor = Color.White;
            }
        }

        #endregion

        #region Buton işlemleri

        private void btnUrunOnayla_Click(object sender, EventArgs e)
        {
            //urunIdsi sıfıra eşit, kullanıcıIdsi null değilse içeri giriyor.
            if (urunId != 0 && kullaniciId != null)
            {
                var adminOnay = new admin();
                //adminUrunOnay fonksiyonuna kullaniciId ve urunId yi gönderiyor.
                adminOnay.adminUrunOnay(kullaniciId, urunId);
                //Datagiridwiewi temizliyor.
                dgUrun.Rows.Clear();
                //Datagiridwiewi tekrardan listeliyor.
                urunIstekleriListele(dgUrun);
                //Verileri resetliyor.


                lblUrun.Text = "";
            }
            //urunIdsi sıfıra eşit, kullanıcıIdsi null ise hata veriyor.
            else
            {
                MessageBox.Show("Lütfen listeden seçim yapınız!", "Seçim Hatası!!!", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            alimEmirKontrol(dataBase.dataBase.alimIstekleri);
        }

        private void btnParaOnayla_Click(object sender, EventArgs e)
        {
            //paraIstekIdsi sıfıra eşit, kullanıcıIdsi null değilse içeri giriyor.
            if (paraIstekId != 0 && kullaniciId != null)
            {
                var adminOnay = new admin();
                //adminParaOnay fonksiyonuna kullaniciId ve paraIstekId yi gönderiyor.
                adminOnay.adminParaOnay(kullaniciId, paraIstekId, paraBirimi);
                //Datagiridwiewi temizliyor.
                dgPara.Rows.Clear();
                //Datagiridwiewi tekrardan listeliyor.
                paraIstekleriListele(dgPara);
                //Verileri resetliyor.
                kullaniciId = null;
                paraIstekId = 0;
                lblPara.Text = "";
            }
            //paraIstekIdsi sıfıra eşit, kullanıcıIdsi null ise hata veriyor.
            else
            {
                MessageBox.Show("Lütfen listeden seçim yapınız!", "Seçim Hatası!!!", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void btnUrunReddet_Click(object sender, EventArgs e)
        {
            //urunIdsi sıfıra eşit, kullanıcıIdsi null değilse içeri giriyor.
            if (urunId != 0 && kullaniciId != null)
            {
                for (var i = 0; i < dataBase.dataBase.kullanicilar.Count; i++)
                    //İlgili kullanıcın ürünleri boş değilse içeri giriyor.
                    if (dataBase.dataBase.kullanicilar[i].urun != null)
                        for (var j = 0; j < dataBase.dataBase.kullanicilar[i].urun.Count; j++)
                            //İligili kullanıcın ilgili ürünü gelen ürünidsine eşit ise içeri giriyor.
                            if (dataBase.dataBase.kullanicilar[i].urun[j].urunId == urunId)
                            {
                                //Reddedilen ürünü siliyor.
                                dataBase.dataBase.kullanicilar[i].urun.RemoveAt(j);
                                MessageBox.Show("Ürün ekleme isteği başarıyla reddedildi");
                                //Datagiridwiewi temizliyor.
                                dgUrun.Rows.Clear();
                                //Datagiridwiewi tekrardan listeliyor
                                urunIstekleriListele(dgUrun);
                                //Verileri resetliyor.
                                kullaniciId = null;
                                urunId = 0;
                                lblUrun.Text = "";
                            }
            }
            //urunIdsi sıfıra eşit, kullanıcıIdsi null ise hata veriyor.
            else
            {
                MessageBox.Show("Lütfen listeden seçim yapınız!", "Seçim Hatası!!!", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void btnParaReddet_Click(object sender, EventArgs e)
        {
            //paraIstekIdsi sıfıra eşit, kullanıcıIdsi null değilse içeri giriyor.
            if (paraIstekId != 0 && kullaniciId != null)
            {
                for (var i = 0; i < dataBase.dataBase.kullanicilar.Count; i++)
                    //İlgili kullanıcın paraIstekleri boş değilse içeri giriyor.
                    if (dataBase.dataBase.kullanicilar[i].paraIstek != null)
                        for (var j = 0; j < dataBase.dataBase.kullanicilar[i].paraIstek.Count; j++)
                            //İligili kullanıcın ilgili paraIdsi gelen paraIstekIdsine eşit ise içeri giriyor.
                            if (dataBase.dataBase.kullanicilar[i].paraIstek[j].paraId == paraIstekId)
                            {
                                //Reddedilen para isteğini siliyor.
                                dataBase.dataBase.kullanicilar[i].paraIstek.RemoveAt(j);
                                MessageBox.Show("Para ekleme isteği başarıyla reddedildi");
                                //Datagiridwiewi temizliyor.
                                dgPara.Rows.Clear();
                                //Datagiridwiewi tekrardan listeliyor
                                paraIstekleriListele(dgPara);
                                //Verileri resetliyor.
                                kullaniciId = null;
                                paraIstekId = 0;
                                lblPara.Text = "";
                            }
            }
            //paraIstekIdsi sıfıra eşit, kullanıcıIdsi null ise hata veriyor.
            else
            {
                MessageBox.Show("Lütfen listeden seçim yapınız!", "Seçim Hatası!!!", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Listelemeler

        private void paraIstekleriListele(DataGridView dg)
        {
            if (dataBase.dataBase.kullanicilar != null)
            {
                for (var i = 0; i < dataBase.dataBase.kullanicilar.Count; i++)
                    //İlgili kullanıcın paraIstekleri boş değilse içeri giriyor.
                    if (dataBase.dataBase.kullanicilar[i].paraIstek != null)
                        for (var j = 0; j < dataBase.dataBase.kullanicilar[i].paraIstek.Count; j++)
                            //Admin onayı false olan para isteklerini listeliyor.
                            if (dataBase.dataBase.kullanicilar[i].paraIstek[j].adminOnay == false)
                                dg.Rows.Add(dataBase.dataBase.kullanicilar[i].paraIstek[j].paraId,
                                    dataBase.dataBase.kullanicilar[i].id,
                                    dataBase.dataBase.kullanicilar[i].paraIstek[j].paraMiktar,
                                    dataBase.dataBase.kullanicilar[i].paraIstek[j].paraBirim);
            }
        }

        private void urunIstekleriListele(DataGridView dg)
        {
            if (dataBase.dataBase.kullanicilar != null)
            {
                for (var i = 0; i < dataBase.dataBase.kullanicilar.Count; i++)
                    //İlgili kullanıcın ürünleri boş değilse içeri giriyor.
                    if (dataBase.dataBase.kullanicilar[i].urun != null)
                        for (var j = 0; j < dataBase.dataBase.kullanicilar[i].urun.Count; j++)
                            //Admin onayı false olan para isteklerini listeliyor..
                            if (dataBase.dataBase.kullanicilar[i].urun[j].adminOnay == false)
                                dgUrun.Rows.Add(dataBase.dataBase.kullanicilar[i].urun[j].urunId,
                                    dataBase.dataBase.kullanicilar[i].id, dataBase.dataBase.kullanicilar[i].urun[j].ad,
                                    dataBase.dataBase.kullanicilar[i].urun[j].miktar,
                                    dataBase.dataBase.kullanicilar[i].urun[j].fiyat);
            }
        }

        #endregion

        //admin bir urune onay verdiginde o urunu sirada bekleyen alimEmirleriyle karsilastiriyoruz
        private void alimEmirKontrol(List<alimIstekleri> istekler)
        {
            urunler urun = dataBase.dataBase.kullanicilar.Where(x => x.id == kullaniciId).FirstOrDefault()
                .urun.Where(y => y.urunId == urunId).FirstOrDefault(); //burada ilgili kullanicinin icinden ilgili urunu urun degiskenine esitledik

            satisIslemleri.satinal(istekler, urun); //alimEmirlerini ve eklenen urunu satinal fonksiyonuna gonderiyoruz

        }
    }
}