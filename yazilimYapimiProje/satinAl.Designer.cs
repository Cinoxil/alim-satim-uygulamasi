﻿
namespace yazilimYapimiProje
{
    partial class satinAl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(satinAl));
            this.dgPazar = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMiktar = new System.Windows.Forms.TextBox();
            this.cmbUrun = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pbUnlem = new System.Windows.Forms.PictureBox();
            this.btnAlimEmir = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFiyat = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgPazar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUnlem)).BeginInit();
            this.SuspendLayout();
            // 
            // dgPazar
            // 
            this.dgPazar.AllowUserToAddRows = false;
            this.dgPazar.AllowUserToDeleteRows = false;
            this.dgPazar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPazar.Location = new System.Drawing.Point(12, 12);
            this.dgPazar.Name = "dgPazar";
            this.dgPazar.ReadOnly = true;
            this.dgPazar.RowHeadersWidth = 51;
            this.dgPazar.Size = new System.Drawing.Size(686, 215);
            this.dgPazar.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(189, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Miktar";
            // 
            // txtMiktar
            // 
            this.txtMiktar.Location = new System.Drawing.Point(241, 247);
            this.txtMiktar.Name = "txtMiktar";
            this.txtMiktar.Size = new System.Drawing.Size(121, 20);
            this.txtMiktar.TabIndex = 1;
            // 
            // cmbUrun
            // 
            this.cmbUrun.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUrun.FormattingEnabled = true;
            this.cmbUrun.Items.AddRange(new object[] {
            "Buğday",
            "Arpa",
            "Yulaf",
            "Pamuk",
            "Patates"});
            this.cmbUrun.Location = new System.Drawing.Point(61, 246);
            this.cmbUrun.Name = "cmbUrun";
            this.cmbUrun.Size = new System.Drawing.Size(121, 21);
            this.cmbUrun.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 249);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ürün";
            // 
            // pbUnlem
            // 
            this.pbUnlem.Image = ((System.Drawing.Image)(resources.GetObject("pbUnlem.Image")));
            this.pbUnlem.Location = new System.Drawing.Point(368, 247);
            this.pbUnlem.Name = "pbUnlem";
            this.pbUnlem.Size = new System.Drawing.Size(21, 20);
            this.pbUnlem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUnlem.TabIndex = 4;
            this.pbUnlem.TabStop = false;
            this.pbUnlem.MouseHover += new System.EventHandler(this.pbUnlem_MouseHover);
            // 
            // btnAlimEmir
            // 
            this.btnAlimEmir.Location = new System.Drawing.Point(595, 243);
            this.btnAlimEmir.Name = "btnAlimEmir";
            this.btnAlimEmir.Size = new System.Drawing.Size(103, 23);
            this.btnAlimEmir.TabIndex = 2;
            this.btnAlimEmir.Text = "Alım Emri Ver";
            this.btnAlimEmir.UseVisualStyleBackColor = true;
            this.btnAlimEmir.Click += new System.EventHandler(this.btnAlimEmir_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(400, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Fiyat";
            // 
            // txtFiyat
            // 
            this.txtFiyat.Location = new System.Drawing.Point(452, 246);
            this.txtFiyat.Name = "txtFiyat";
            this.txtFiyat.Size = new System.Drawing.Size(121, 20);
            this.txtFiyat.TabIndex = 1;
            // 
            // satinAl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 283);
            this.Controls.Add(this.btnAlimEmir);
            this.Controls.Add(this.pbUnlem);
            this.Controls.Add(this.cmbUrun);
            this.Controls.Add(this.txtFiyat);
            this.Controls.Add(this.txtMiktar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgPazar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "satinAl";
            this.Text = "alisSatis";
            this.Load += new System.EventHandler(this.satinAl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgPazar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUnlem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgPazar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMiktar;
        private System.Windows.Forms.ComboBox cmbUrun;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbUnlem;
        private System.Windows.Forms.Button btnAlimEmir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFiyat;
    }
}