﻿using System;
using System.Windows.Forms;
using yazilimYapimiProje.PanelIslem;

namespace yazilimYapimiProje
{
    public partial class frmKayitOl : Form
    {
        public frmKayitOl()
        {
            InitializeComponent();
        }

        private void btnKayitOl_Click(object sender, EventArgs e)
        {
            //Texboxlardan ve comboboxlardan bilgiler alınıp kullanıcı classı altındaki kullaniciKayit fonsiyonuna gönderiliyor.
            panelIslemleri.kullaniciKayit(txtKullaniciAdi.Text, txtSifre.Text, txtAd.Text, txtSoyad.Text, mTxtTc.Text,
                mTxtTelefon.Text, txtMail.Text, rTxtAdres.Text);
        }

        private void btnGeri_Click(object sender, EventArgs e)
        {
            panelIslemleri.oturumuKapat();
        }
    }
}