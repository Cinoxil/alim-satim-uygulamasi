﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using yazilimYapimiProje.Entities;
using yazilimYapimiProje.PanelIslem;

namespace yazilimYapimiProje
{
    public partial class satinAl : Form
    {
        public satinAl()
        {
            InitializeComponent();
        }

        private void pbUnlem_MouseHover(object sender, EventArgs e)
        {
            var ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(pbUnlem, "Lütfen miktarı kg cinsinde giriniz");
        }

        private void satinAl_Load(object sender, EventArgs e)
        {
            dgPazar.AllowUserToDeleteRows = false; //dgPazar data gridinin columlarini olusturuyoruz
            //Dtatgridwievın sütun sayısını ve sütun isimlerini belirliyor.
            dgPazar.ColumnCount = 3;
            dgPazar.Columns[0].Name = "Ürün";
            dgPazar.Columns[1].Name = "Miktar";
            dgPazar.Columns[2].Name = "Fiyat";
            urunler.urunleriListele(dgPazar);
        }

        

        private void btnAlimEmir_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFiyat.Text) && !string.IsNullOrEmpty(cmbUrun.Text) && !string.IsNullOrEmpty(txtMiktar.Text) && textBoxSayiControl())
            {
                satisIslemleri.satinal(cmbUrun.Text, Convert.ToInt32(txtMiktar.Text), Convert.ToInt32(txtFiyat.Text),dgPazar);
                //Miktarı sıfır olan ürünleri, silen fonksiyomnu çalıştırıyor.
                urunler.urunSil();
                dgPazar.Rows.Clear();
                urunler.urunleriListele(dgPazar);
                panelIslemleri.panelYenile();
            }
            else
            {
                //combox ve miktar textbox boş değilse içeri giriyor.
                if (!string.IsNullOrEmpty(cmbUrun.Text) && !string.IsNullOrEmpty(txtMiktar.Text) && textBoxSayiControl())
                {
                    satisIslemleri.satinal(cmbUrun.Text, Convert.ToInt32(txtMiktar.Text),dgPazar);
                    //Miktarı sıfır olan ürünleri, silen fonksiyomnu çalıştırıyor.
                    urunler.urunSil();
                    dgPazar.Rows.Clear();
                    urunler.urunleriListele(dgPazar);
                    panelIslemleri.panelYenile();
                }
                //combox ve miktar textbox boşsa hata veriyor.
                else
                {
                    MessageBox.Show("Lütfen gerekli alanları doldurunuz");
                }

            }
        }

        //Miktar girdisi sayı mı diye kontrol ediyor.
        private bool textBoxSayiControl()
        {
            //Miktar girdisi sayı ise txtKontrol true oluyor
            var txtKontrol = txtMiktar.Text.All(char.IsDigit);
            //txtKontrol false ise hata veriyor.
            if (!txtKontrol) MessageBox.Show("Lütfen sayısal değer giriniz");

            return txtKontrol;
        }

       
    }
}
