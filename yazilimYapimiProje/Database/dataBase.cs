﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using yazilimYapimiProje.Entities;

namespace yazilimYapimiProje.dataBase
{
    public static class dataBase
    {
        public static string gecerliKullanici;

        public static List<admin> adminler = new List<admin>(); //Adminleri tutan admin listesi.

        public static List<kullanici> kullanicilar = new List<kullanici>(); //Kullanıcıları tutan kullanıcı listesi.

        public static List<alimIstekleri> alimIstekleri = new List<alimIstekleri>(); //istenilen fiyattan otomatik satin alma yapilmasi icin tuttugumuz field

        public static void serializeJSON()
        {
            var json = JsonConvert.SerializeObject(kullanicilar); //Kullanıcılar listesini jsona çevirip json değişkenine eşitliyor.
            File.WriteAllText(@".\Kullanicilar.json", json); //Kullanicilar.json adında bir dosya oluşturup json değişkenini o dosyaya yazıyor.
            
            var adminJson = JsonConvert.SerializeObject(adminler); //Kullanıcılar listesini jsona çevirip json değişkenine eşitliyor.
            File.WriteAllText(@".\Adminler.json", adminJson); //Kullanicilar.json adında bir dosya oluşturup json değişkenini o dosyaya yazıyor.

            var isteklerJson = JsonConvert.SerializeObject(alimIstekleri); //Kullanıcılar listesini jsona çevirip json değişkenine eşitliyor.
            File.WriteAllText(@".\Istekler.json", isteklerJson); //Kullanicilar.json adında bir dosya oluşturup json değişkenini o dosyaya yazıyor.
        }

        public static void deSerializeJSON()
        {
            var jsonVerisi = ""; //Null hatası vermemesi için boş default değer atıyor.
            jsonVerisi =
                File.ReadAllText(
                    @".\Kullanicilar.json"); //Kullanicilar.json okuyup içindekileri jsonVerisi değişkenine atıyor.
            //jsonVerisini list kullanıcı tipine dönüştürüp kullancılar listesine atıyor.
            kullanicilar = JsonConvert.DeserializeObject<List<kullanici>>(jsonVerisi);

            var adminJsonVerisi = ""; //Null hatası vermemesi için boş default değer atıyor.
            adminJsonVerisi =
                File.ReadAllText(
                    @".\Adminler.json"); //Kullanicilar.json okuyup içindekileri jsonVerisi değişkenine atıyor.
            //jsonVerisini list kullanıcı tipine dönüştürüp kullancılar listesine atıyor.
            adminler = JsonConvert.DeserializeObject<List<admin>>(adminJsonVerisi);

            var isteklerJsonVerisi = ""; //Null hatası vermemesi için boş default değer atıyor.
            isteklerJsonVerisi =
                File.ReadAllText(
                    @".\Istekler.json"); //Kullanicilar.json okuyup içindekileri jsonVerisi değişkenine atıyor.
            //jsonVerisini list kullanıcı tipine dönüştürüp kullancılar listesine atıyor.
            alimIstekleri = JsonConvert.DeserializeObject<List<alimIstekleri>>(isteklerJsonVerisi);
        }
    }
}