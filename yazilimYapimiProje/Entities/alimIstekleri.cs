﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yazilimYapimiProje.Entities
{
    //otamatik alim icin database'e alimIstekleri class'i tipinde nesneleri kaydedip daha sonra satilik urunlerle karsilastiriyoruz
    public class alimIstekleri
    {
        public kullanici alici { get; set; } //hangi kullanicinin ekledigini belirlemek icin kullanici tipinde alici fieldi tutuyoruz
        public string urunAdi { get; set; }
        public int miktar { get; set; }
        public int birimFiyat { get; set; }

        public alimIstekleri(kullanici _alici, string _urunAdi, int _miktar, int _birimFiyat)
        {
            alici = _alici;
            urunAdi = _urunAdi;
            miktar = _miktar;
            birimFiyat = _birimFiyat;
        }

    }
}
