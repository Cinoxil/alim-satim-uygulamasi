﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using yazilimYapimiProje.PanelIslem;

namespace yazilimYapimiProje.Entities
{
    public class urunler
    {
        private static int sayac = 1; //Ürün idsi tutmak için sayaç fieldi oluşturuldu.
        public bool eklemeGecerlilik = true;

        public string sahibi = dataBase.dataBase.gecerliKullanici; //Sahibinin belirtmek için oluşturulan field.
        public DateTime satinAlmaTarihi { get; set; }

        //Yapıcı methoda ad, miktar, fiyat değişkenleri geliyor, ilgili fieldler birbirine eşitleniyor.
        public urunler(string ad, int miktar, int fiyat)
        {
            this.ad = ad;
            this.miktar = miktar;
            this.fiyat = fiyat;
            adminOnay = false;
            urunId = sayac;
        }


        private int _urunId { get; set; }
        private string _ad { get; set; }
        private int _miktar { get; set; }
        private int _fiyat { get; set; }
        public bool adminOnay { get; set; }


        #region Urun ekleme

        //Urunler tipinde u nesnesini alıyor.
        public static void urunEkle(urunler u)
        {
            var frmUrunEkle = Application.OpenForms["urunEkle"];
            if (frmUrunEkle != null)
            {
                var gb = (GroupBox)frmUrunEkle.Controls["gbUrunTxt"];
                //Geçerli kullacının içindeki ürün listesi null değilse buraya girer.
                if (dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici)
                    .FirstOrDefault().urun != null)
                {
                    //Geçerlikullanıcının içindeki ürün listesine u nesnesini ekler.
                    dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici)
                        .FirstOrDefault()
                        .urun.Add(u);
                    MessageBox.Show("Ürün ekleme isteğiniz başarıyla alınmıştır");
                    panelIslemleri.formTemizle(gb);
                }
                else //Geçerli kullacının içindeki ürün listesi null ise buraya girer.
                {
                    //Geçerlikullanıcın içindeki ürün listesi null olduğu için listeyi örnekler.
                    dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici)
                        .FirstOrDefault().urun = new List<urunler>();
                    //Geçerlikullanıcının içindeki ürün listesine u nesnesini ekler.
                    dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici)
                        .FirstOrDefault()
                        .urun.Add(u);
                    MessageBox.Show("Ürün ekleme isteğiniz başarıyla alınmıştır");
                    panelIslemleri.formTemizle(gb);
                }
            }
        }

        #endregion

        #region Tukenen urunleri listeden silme

        //Miktarı sıfırdan fazla olan ürünleri siler.
        public static void urunSil()
        {
            for (var i = 0; i < dataBase.dataBase.kullanicilar.Count; i++)
                //Geçerli kullanıcı içindeki ürün listesi null değil ise çalışır.
                if (dataBase.dataBase.kullanicilar[i].urun != null)
                    for (var j = 0; j < dataBase.dataBase.kullanicilar[i].urun.Count; j++)
                        //Eğer geçerli kullanıcı içindeki geçerli ürün miktarı sıfıra eşitse içeri girer.
                        if (dataBase.dataBase.kullanicilar[i].urun[j].miktar == 0)
                            //ürünü listeden removeat ile indeksine göre siler.
                            dataBase.dataBase.kullanicilar[i].urun.RemoveAt(j);
        }

        #endregion

        #region Properties

        public int urunId
        {
            get => _urunId;
            set //Sayaçtan gelen value ürün idye eşitleniyor ve sayaç bir arttırıyor.
            {
                _urunId = value;
                sayac++;
            }
        }

        public string ad
        {
            get => _ad;
            set
            {
                if (value == null) //Gelen ad null ise hata veriyor ve ekleme geçerlilik null oluyor.
                {
                    MessageBox.Show("Lütfen ekleyeceğiniz ürünü seçiniz");
                    eklemeGecerlilik = false;
                }
                else
                {
                    _ad = value;
                }
            }
        }

        public int miktar
        {
            get => _miktar;
            set
            {
                if (value < 0) //Ürün miktar sıfırdan küçükse hata veriyor ve eklemegeçerlilik false oluyor.
                {
                    MessageBox.Show("Lütfen geçerli bir miktar giriniz");
                    eklemeGecerlilik = false;
                }
                else
                {
                    _miktar = value;
                }
            }
        }

        public int fiyat
        {
            get => _fiyat;
            set
            {
                if (value <= 0) //Ürün fiyat sıfırdan küçük veya eşitse hata veriyor ve eklemegeçerlilik false oluyor.
                {
                    MessageBox.Show("Lütfen geçerli bir fiyat giriniz");
                    eklemeGecerlilik = false;
                }
                else
                {
                    _fiyat = value;
                }
            }
        }

        #endregion

        #region Urun listeleme

        //Ürünlerin listeleneceği yer geçerli datagridwievu dg nesnesi ile fonksiyona alıyor.
        public static void urunleriListele(DataGridView dg)
        {
            //Kullanıcı sayısı kadar dönüyor.
            for (var i = 0; i < dataBase.dataBase.kullanicilar.Count; i++)
                //Geçerli kullanıcın ürünlerini listelememek için bu if şartını yazdık.
                if (dataBase.dataBase.kullanicilar[i].id != dataBase.dataBase.gecerliKullanici)
                    //Eğer kullanıcılar[i]nin içindeki ürün listesi null değilse içeri girer.
                    if (dataBase.dataBase.kullanicilar[i].urun != null)
                        for (var j = 0; j < dataBase.dataBase.kullanicilar[i].urun.Count; j++)
                            //Adminonayı true olan ve ürün miktarı sıfırdan büyük mü diye kontrol ediyor.
                            if (dataBase.dataBase.kullanicilar[i].urun[j].adminOnay &&
                                dataBase.dataBase.kullanicilar[i].urun[j].miktar > 0)
                                //Lİsteleme işlemini gerçekleştiriyor.
                                dg.Rows.Add(dataBase.dataBase.kullanicilar[i].urun[j].ad,
                                    dataBase.dataBase.kullanicilar[i].urun[j].miktar,
                                    dataBase.dataBase.kullanicilar[i].urun[j].fiyat);
        }

        #region Gecerli kullanici urunleri listeleme

        //Ürünlerin listeleneceği yer geçerli datagridwievu dg nesnesi ve geçerliKullaniciyi fonksiyona alıyor.
        public static void urunleriListele(DataGridView dg, string gecerliKullanici)
        {
            //kişi nesnesini geçerli kullanıcı nesnesini eşitliyor.
            kullanici kisi = dataBase.dataBase.kullanicilar.Where(x => x.id == gecerliKullanici).FirstOrDefault();

            if (kisi.urun != null) //Geçerli kişinin içindeki ürün listesi null değil ise buraya giriyor.
            {
                for (var i = 0; i < kisi.urun.Count; i++)
                {
                    //Adminonayı true ve miktarı sıfıradan fazla olan ürünleri listeler.
                    if (kisi.urun[i].adminOnay && kisi.urun[i].miktar > 0)
                    {
                        dg.Rows.Add(kisi.urun[i].ad, kisi.urun[i].miktar, kisi.urun[i].fiyat, kisi.urun[i].satinAlmaTarihi.ToShortDateString());
                    }
                }
            }
            if (kisi.satinAlinanUrunler != null) //Geçerli kişinin içindeki satinAlinanUrunler listesi null değil ise buraya giriyor.
            {
                for (var i = 0; i < kisi.satinAlinanUrunler.Count; i++)
                {
                    //Adminonayı true ve miktarı sıfıradan fazla olan ürünleri listeler.
                    if (kisi.satinAlinanUrunler[i].adminOnay && kisi.satinAlinanUrunler[i].miktar > 0)
                    {
                        dg.Rows.Add(kisi.satinAlinanUrunler[i].ad, kisi.satinAlinanUrunler[i].miktar, kisi.satinAlinanUrunler[i].fiyat, kisi.satinAlinanUrunler[i].satinAlmaTarihi.ToShortDateString());
                    }
                }
            }


        }

        public static void urunleriListele(DataGridView dg, string gecerliKullanici, DateTime baslangic, DateTime bitis)
        {
            //kişi nesnesini geçerli kullanıcı nesnesini eşitliyor.
            kullanici kisi = dataBase.dataBase.kullanicilar.Where(x => x.id == gecerliKullanici).FirstOrDefault();
            List<urunler> satinAlinanlar = new List<urunler>();

            if (kisi.satinAlinanUrunler != null) //Geçerli kişinin içindeki satinAlinanUrunler listesi null değil ise buraya giriyor.
            {
                satinAlinanlar.AddRange(kisi.satinAlinanUrunler.Where(x => x.satinAlmaTarihi <= bitis && x.satinAlmaTarihi >= baslangic).ToList());

                for (int i = 0; i < satinAlinanlar.Count; i++)
                {
                    //Adminonayı true ve miktarı sıfıradan fazla olan ürünleri listeler.
                    if (satinAlinanlar[i].miktar > 0)
                    {
                        dg.Rows.Add(satinAlinanlar[i].ad, satinAlinanlar[i].miktar, satinAlinanlar[i].fiyat, satinAlinanlar[i].satinAlmaTarihi.ToShortDateString());
                    }
                }
            }


        }



        #endregion

        #endregion
    }
}