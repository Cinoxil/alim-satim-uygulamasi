﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using yazilimYapimiProje.PanelIslem;

namespace yazilimYapimiProje.Entities
{
    public static class satisIslemleri
    {

        #region satin alma emri

        //Satın alma işlemini gerçekleştiren algoritmanın bulunduğu fonksiyon.
        private static kullanici alici = dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault();


        public static void satinal(string urunAdi, int alinacakMiktar, DataGridView dgPazar)
        {
            //urunListesi adında yeni bir temp liste oluşturuyor.
            var urunListesi = new List<urunler>();
            //Satın almak isteyen kullanıcıyı alici isimli nesneye çekiyoruz.

            for (var i = 0; i < dataBase.dataBase.kullanicilar.Count; i++)
            {

                //Kendisine ait olan ürünleri satın almaması için kontrol yapıyor.
                if (dataBase.dataBase.gecerliKullanici != dataBase.dataBase.kullanicilar[i].id)
                {
                    if (dataBase.dataBase.kullanicilar[i].urun != null)
                    {
                        //urunlistesine alınmak istene ürünleri ismine göre ekliyor.
                        urunListesi.AddRange(
                            dataBase.dataBase.kullanicilar[i].urun.Where(x => x.ad == urunAdi).ToList());
                        for (var k = 0; k < dataBase.dataBase.kullanicilar[i].urun.Count; k++)
                            //Ürünleri ürün listesine çektikten sonra ürün sahiplarinin listesinden geçici olarak siliyor.
                            if (dataBase.dataBase.kullanicilar[i].urun[k].ad == urunAdi)
                            {
                                dataBase.dataBase.kullanicilar[i].urun.RemoveAt(k);
                                k--;
                            }
                    }

                    //urunListesine çekilen ürünleri ucuzdan pahalıya sıralıyor.
                    urunListesi.Sort(
                        delegate (urunler u1, urunler u2) { return u1.fiyat.CompareTo(u2.fiyat); });

                }

            }

            satisIslemi(urunListesi, alinacakMiktar, dgPazar);
        }

        public static void satinal(string urunAdi, int alinacakmiktar, int istenilenFiyat, DataGridView dgPazar)
        {
            //urunListesi adında yeni bir temp liste oluşturuyor.
            var urunListesi = new List<urunler>();
            //Satın almak isteyen kullanıcıyı alici isimli nesneye çekiyoruz.

            for (var i = 0; i < dataBase.dataBase.kullanicilar.Count; i++) //databasede kullanicilar listesinde geziyoruz
            {
                if (dataBase.dataBase.kullanicilar[i].urun != null) //eger ilgili kullanicinin urun listesi bos degilse devam eder
                {
                    for (int j = 0; j < dataBase.dataBase.kullanicilar[i].urun.Count; j++) //ilgili kullanicinin urunlerinde geziyoruz
                    {
                        if (dataBase.dataBase.kullanicilar[i].urun[j].adminOnay == true) //eger ilgili urunun admin onayi true ise devam ediyor
                        {

                            if (dataBase.dataBase.gecerliKullanici != dataBase.dataBase.kullanicilar[i].id) //gecerliKullanici databasede urunlerine baktigimiz kullanicimi diye kontrol
                                                                                                            //ediyoruz degilse devam ediyoruz (kendi urunlerini satin almamasi icin)
                            {
                                if (dataBase.dataBase.kullanicilar[i].urun[j].ad == urunAdi) //eger kullanicinin urunlerindeki urun adi fonksiyona gelen urun adi ise devam ediyor
                                {
                                    if (istenilenFiyat == dataBase.dataBase.kullanicilar[i].urun[j].fiyat) //alicinin satin almak istedigi fiyat ile saticinin satis fiyati esit mi diye bakiyor
                                    {
                                        urunListesi.Add(dataBase.dataBase.kullanicilar[i].urun[j]); //yukarida olusturdugumuz temp urunListesine urunu ekliyoruz 
                                        dataBase.dataBase.kullanicilar[i].urun.RemoveAt(j); //buradada daha sonra iade etmek uzere saticidan urunu sildik
                                        j--; //urunu sildigimizde index kaydigi icin j'yi 1 eksiltiyoruz
                                    }

                                }
                            }

                        }
                    }

                }
            }

            if (urunListesi.Count == 0) //urunListesine hic urun eklenmediyse alimEmri olusturuluyor
            {
                alimIstekleri a = new alimIstekleri(dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault(),
                    urunAdi, alinacakmiktar, istenilenFiyat); //alimIsteklerinin ctor func istediklerini gonderiyoruz
                if (dataBase.dataBase.alimIstekleri != null) //eger databasedeki alimIstekleri listesi bos degilse buraya girer
                {
                    dataBase.dataBase.alimIstekleri.Add(a); //yukarida olusturulan a nesnesi databasedeki alimIstekleri listesine ekler
                    MessageBox.Show(alinacakmiktar.ToString() + " adet " + urunAdi + " alim isteginiz kayit edilmistir");
                    panelIslemleri.formTemizle(Application.OpenForms["satinAl"]); //panelislemlerindeki formTemizle fonksiyonu ile formu temizliyoruz

                }
                else
                {

                    dataBase.dataBase.alimIstekleri = new List<alimIstekleri>(); //alimIsteklerini ornekliyor ve daha sonra nesneyi ekliyor 
                    dataBase.dataBase.alimIstekleri.Add(a);
                    MessageBox.Show(alinacakmiktar.ToString() + " adet " + urunAdi + " alim isteginiz kayit edilmistir");
                    panelIslemleri.formTemizle(Application.OpenForms["satinAl"]);
                }
            }
            else
            {
                satisIslemi(urunListesi, alinacakmiktar, dgPazar); //istenilen urun istenilen fiyatta bulundugu icin direk satin alim yapar
            }
        }

        public static void satinal(List<alimIstekleri> istekler, urunler urun)
        {
            //urunListesi adında yeni bir temp liste oluşturuyor.
            var urunListesi = new List<urunler>();
            if (istekler != null)
            {

                for (int i = 0; i < istekler.Count; i++)
                {

                    if (istekler[i].urunAdi == urun.ad)
                    {
                        if (istekler[i].birimFiyat == urun.fiyat)
                        {
                            double odenecekPara = 0;

                            var alinabilecekMiktar = (istekler[i].alici.para / urun.fiyat); //alıcının parasına göre max. alabileciği miktarı hesaplıyor.
                            double komisyon = komisyonHesapla(urun.miktar, urun.fiyat, istekler[i].miktar, alinabilecekMiktar);
                            alinabilecekMiktar -= komisyon / urun.fiyat;

                            //Eğer alıcının parası satın alım işlemine yatmiyorsa hata veriyor.
                            if ((int)alinabilecekMiktar == 0)
                            {
                            }
                            //Eğer alıcının almak istediği miktar satılan en ucuz ürünün miktarından küçük veya eşitse buraya giriyor.
                            else if (istekler[i].miktar <= urun.miktar)
                            {
                                //Eğer alıcının almak istediği miktar alabileceği miktardan küçük veya eşitse buraya giriyor.
                                if (istekler[i].miktar <= (int)alinabilecekMiktar)
                                {
                                    urun.miktar -= istekler[i].miktar;
                                    odenecekPara += (urun.fiyat * istekler[i].miktar); //Ödenecek parayı hesaplıyor.
                                    aliciyaUrunAktar(urun.ad, istekler[i].miktar, urun.fiyat);
                                    istekler[i].miktar = 0;

                                    /*Alıcının ve satıcın para miktarını düzenleyen paraMiktarAyar fonksiyonuna gerekli değişkenler yollanıyor ve alıcı
                                  paraya eşitleniyor.*/
                                    istekler[i].alici.para = paraMiktarAyar(istekler[i].alici.para, odenecekPara, (int)alinabilecekMiktar,
                                       urun.sahibi, urun.fiyat, komisyon);
                                    //Alınmakl istene  miktar alındığı için break oluyor.
                                    break;
                                }
                                //Eğer alıcının almak istediği miktar alabileceği miktardan küçük veya eşit değilse buraya giriyor.
                                else
                                {
                                    urun.miktar -= (int)alinabilecekMiktar;
                                    odenecekPara += urun.fiyat * (int)alinabilecekMiktar;
                                    aliciyaUrunAktar(urun.ad, istekler[i].miktar, urun.fiyat);
                                    istekler[i].miktar = 0;
                                    istekler[i].alici.para = paraMiktarAyar(istekler[i].alici.para, odenecekPara, (int)alinabilecekMiktar, urun.sahibi,
                                        urun.fiyat, komisyon);
                                }

                            }
                            //Eğer alıcının almak istediği miktar satılan en ucuz ürünün miktarından küçük veya eşit değilse buraya giriyor.
                            else
                            {
                                //Eğer alıcının alabileceği miktar satılan en ucuz miktardan küçük veya eşitse buraya giriyor.
                                if (alinabilecekMiktar <= urun.miktar)
                                {
                                    urun.miktar -= (int)alinabilecekMiktar;
                                    odenecekPara += (int)alinabilecekMiktar * urun.fiyat;
                                    aliciyaUrunAktar(urun.ad, (int)alinabilecekMiktar, urun.fiyat);
                                    alinabilecekMiktar = 0;
                                    istekler[i].alici.para = paraMiktarAyar(istekler[i].alici.para, odenecekPara, (int)alinabilecekMiktar,
                                        urun.sahibi, urun.fiyat, komisyon);
                                }
                                //Eğer alıcının alabileceği miktar satılan en ucuz miktardan büyükse buraya giriyor.
                                else
                                {
                                    istekler[i].miktar -= urun.miktar;
                                    odenecekPara += urun.fiyat * urun.miktar;
                                    aliciyaUrunAktar(urun.ad, urun.miktar, urun.fiyat);
                                    urun.miktar = 0; ;
                                    istekler[i].alici.para = paraMiktarAyar(istekler[i].alici.para, odenecekPara, (int)alinabilecekMiktar,
                                        urun.sahibi, urun.fiyat, komisyon);

                                }
                            }

                            //Alıcının almak istediği miktar sıfır olduğunda buraya giriyor ve berak oluyor.
                            if (istekler[i].miktar == 0) break;
                        }
                        //urunListesindeki ürünlei sahiplerine geri iade ediyor.
                        for (var j = 0; j < urunListesi.Count; j++)
                        {
                            dataBase.dataBase.kullanicilar.Where(x => x.id == urunListesi[j].sahibi).FirstOrDefault().urun
                                .Add(urunListesi[j]);
                            panelIslemleri.formTemizle(Application.OpenForms["satinAl"]);

                        }
                    }
                }
            }

        }


        private static void satisIslemi(List<urunler> urunListesi, int alinacakMiktar, DataGridView dgPazar)
        {
            kullanici alici = dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault();
            for (var j = 0; j < urunListesi.Count; j++)
            {
                if (urunListesi[j].adminOnay != false)
                {
                    double odenecekPara = 0;
                    if (alici != null)
                    {

                        var alinabilecekMiktar = (alici.para / urunListesi[j].fiyat); //alıcının parasına göre max. alabileciği miktarı hesaplıyor.
                        double komisyon = komisyonHesapla(urunListesi[j].miktar, urunListesi[j].fiyat, alinacakMiktar, alinabilecekMiktar);
                        alinabilecekMiktar -= komisyon / urunListesi[j].fiyat;

                        //Eğer alıcının parası satın alım işlemine yatmiyorsa hata veriyor.
                        if ((int)alinabilecekMiktar == 0)
                        {
                            MessageBox.Show("Yeterli paranız bulunmamaktadır");
                        }
                        //Eğer alıcının almak istediği miktar satılan en ucuz ürünün miktarından küçük veya eşitse buraya giriyor.
                        else if (alinacakMiktar <= urunListesi[j].miktar)
                        {
                            //Eğer alıcının almak istediği miktar alabileceği miktardan küçük veya eşitse buraya giriyor.
                            if (alinacakMiktar <= (int)alinabilecekMiktar)
                            {
                                urunListesi[j].miktar -= alinacakMiktar;
                                odenecekPara += (urunListesi[j].fiyat * alinacakMiktar); //Ödenecek parayı hesaplıyor.
                                aliciyaUrunAktar(urunListesi[j].ad, alinacakMiktar, urunListesi[j].fiyat);
                                alinacakMiktar = 0;

                                dgPazar.Rows.Clear();
                                urunler.urunleriListele(dgPazar);
                                /*Alıcının ve satıcın para miktarını düzenleyen paraMiktarAyar fonksiyonuna gerekli değişkenler yollanıyor ve alıcı
                              paraya eşitleniyor.*/
                                alici.para = paraMiktarAyar(alici.para, odenecekPara, (int)alinabilecekMiktar,
                                    urunListesi[j].sahibi, urunListesi[j].fiyat, komisyon);
                                MessageBox.Show("Satın alım işlemi başarılı");
                                //Alınmakl istene  miktar alındığı için break oluyor.
                                break;
                            }
                            //Eğer alıcının almak istediği miktar alabileceği miktardan küçük veya eşit değilse buraya giriyor.
                            else
                            {
                                urunListesi[j].miktar -= (int)alinabilecekMiktar;
                                odenecekPara += urunListesi[j].fiyat * (int)alinabilecekMiktar;
                                aliciyaUrunAktar(urunListesi[j].ad, alinacakMiktar, urunListesi[j].fiyat);
                                alinacakMiktar = 0;
                                alici.para = paraMiktarAyar(alici.para, odenecekPara, (int)alinabilecekMiktar, urunListesi[j].sahibi,
                                    urunListesi[j].fiyat, komisyon);
                                dgPazar.Rows.Clear();
                                urunler.urunleriListele(dgPazar);
                                MessageBox.Show("Satın alım işlemi başarılı");
                            }

                        }
                        //Eğer alıcının almak istediği miktar satılan en ucuz ürünün miktarından küçük veya eşit değilse buraya giriyor.
                        else
                        {
                            //Eğer alıcının alabileceği miktar satılan en ucuz miktardan küçük veya eşitse buraya giriyor.
                            if (alinabilecekMiktar <= urunListesi[j].miktar)
                            {
                                urunListesi[j].miktar -= (int)alinabilecekMiktar;
                                odenecekPara += (int)alinabilecekMiktar * urunListesi[j].fiyat;
                                aliciyaUrunAktar(urunListesi[j].ad, (int)alinabilecekMiktar, urunListesi[j].fiyat);
                                alinabilecekMiktar = 0;
                                alici.para = paraMiktarAyar(alici.para, odenecekPara, (int)alinabilecekMiktar,
                                    urunListesi[j].sahibi, urunListesi[j].fiyat, komisyon);
                                dgPazar.Rows.Clear();
                                urunler.urunleriListele(dgPazar);
                                MessageBox.Show("Satın alım işlemi başarılı");
                            }
                            //Eğer alıcının alabileceği miktar satılan en ucuz miktardan büyükse buraya giriyor.
                            else
                            {
                                alinacakMiktar -= urunListesi[j].miktar;
                                odenecekPara += urunListesi[j].fiyat * urunListesi[j].miktar;
                                aliciyaUrunAktar(urunListesi[j].ad, urunListesi[j].miktar, urunListesi[j].fiyat);
                                urunListesi[j].miktar = 0;
                                dgPazar.Rows.Clear();
                                alici.para = paraMiktarAyar(alici.para, odenecekPara, (int)alinabilecekMiktar,
                                    urunListesi[j].sahibi, urunListesi[j].fiyat, komisyon);
                                urunler.urunleriListele(dgPazar);
                            }
                        }
                    }

                    //Alıcının almak istediği miktar sıfır olduğunda buraya giriyor ve berak oluyor.
                    if (alinacakMiktar == 0) break;
                }


            }
            //urunListesindeki ürünlei sahiplerine geri iade ediyor.
            for (var i = 0; i < urunListesi.Count; i++)
            {
                dataBase.dataBase.kullanicilar.Where(x => x.id == urunListesi[i].sahibi).FirstOrDefault().urun
                    .Add(urunListesi[i]);
                panelIslemleri.formTemizle(Application.OpenForms["satinAl"]);

            }
        }
        #endregion

        #region Satici ve alici para miktari duzenlenmesi

        //Fonksiyona gelen gerekli değişkenler ile satış yapan kullanıcılara ödemeleri yapılıyor ve alıcılardan para kesiliyor.
        private static double paraMiktarAyar(double aliciPara, double odenecekPara, int alinabilecekmiktar, string sahibi, int urunFiyat, double komisyon)
        {

            //Alıcının parası ödeyeceği paradan büyükse buraya giriyor.
            if (odenecekPara <= aliciPara)
            {
                //satışı yapan kişiye ödemesi gerçekleştiriliyor.
                dataBase.dataBase.kullanicilar.Where(x => x.id == sahibi).FirstOrDefault().para += odenecekPara;
                //Alıcıdan ödediği para kesiliyor.
                aliciPara -= (odenecekPara + komisyon);
                dataBase.dataBase.adminler.Where(x => x.id == "admin").FirstOrDefault().komisyonPara += komisyon;
                odenecekPara = 0;
            }
            //Alıcının parası ödeyeceği paradan küçükse buraya giriyor.
            else
            {
                //Parası yetmediği durumda max. alabileceği miktarın parası hesaplanıyor.
                odenecekPara = urunFiyat * alinabilecekmiktar;
                //satışı yapan kişiye ödemesi gerçekleştiriliyor.
                dataBase.dataBase.kullanicilar.Where(x => x.id == sahibi).FirstOrDefault().para += odenecekPara;
                //Alıcıdan ödediği para kesiliyor.
                aliciPara -= (odenecekPara + komisyon);
                dataBase.dataBase.adminler.Where(x => x.id == "admin").FirstOrDefault().komisyonPara += komisyon;
                odenecekPara = 0;
            }

            return aliciPara;
        }

        #endregion

        //komisyon hesaplamak icin kullandigimiz fonksiyon
        private static double komisyonHesapla(int urunMiktar, int urunFiyat, int alinacakMiktar, double alinabilecekMiktar)
        {
            const double komisyonOrani = 0.01; //%1 komisyon oranimizi const double olarak tutuyoruz
            double komisyon = 0; //return etmek icin temp bir degisken

            if (alinabilecekMiktar == 0) //eger alinabilecek miktar 0 ise komisyonda 0'dir
            {
                komisyon = 0;
            }
            else if (alinacakMiktar <= urunMiktar) //alinacakMiktar satilan urunMiktarindan kucuk yada esitse buraya girer
            {
                if (alinacakMiktar <= alinabilecekMiktar) //alinacakMiktar alinabilecekMiktardan kucuk yada esitse buraya girer
                {
                    komisyon = alinacakMiktar * urunFiyat * komisyonOrani;
                }
                else //alinacakMiktar alinabilecekMiktardan buyuk yada esitse buraya girer
                {
                    komisyon = alinabilecekMiktar * urunFiyat * komisyonOrani;
                }
            }
            else
            {
                if (alinabilecekMiktar <= urunMiktar) //alinabilecekMiktar satilan urunMiktarindan kucuk yada esitse buraya girer
                {
                    komisyon = alinabilecekMiktar * urunFiyat * komisyonOrani;
                }
                else //alinabilecekMiktar satilan urunMiktarindan buyuk yada esitse buraya girer
                {
                    komisyon = urunMiktar * urunFiyat * komisyonOrani;
                }
            }
            return komisyon;
        }
        private static void aliciyaUrunAktar(string ad, int miktar, int fiyat)
        {
            if (alici.satinAlinanUrunler == null) //eger alicinin icindeki satinAlinanUrunler listesi bos ise ornekleme icin buraya girer
            {
                alici.satinAlinanUrunler = new List<urunler>(); //satinAlinanUrunler listesini ornekliyor
                urunler u = new urunler(ad, miktar, fiyat); //aliciya aktarilacak urunu yaratiyoruz
                u.satinAlmaTarihi = DateTime.Now; //satinAlmaTarihini simdi olarak ilgili fielda esitliyor
                u.adminOnay = true;
                alici.satinAlinanUrunler.Add(u); // satinAlinanUrunler listesine ekliyor
            }
            else //eger daha onceden satinAlinanUrunler listesi orneklenmisse buraya girer
            {
                urunler u = new urunler(ad, miktar, fiyat);
                u.adminOnay = true;
                u.satinAlmaTarihi = DateTime.Now;
                alici.satinAlinanUrunler.Add(u);
            }


        }
    }
}
