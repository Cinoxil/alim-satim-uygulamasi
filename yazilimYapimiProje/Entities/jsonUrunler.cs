﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace yazilimYapimiProje.Entities
{
    class jsonUrunler
    {
        static private int sayac = 1;
        private int urunId { get; set; }
        private string ad { get; set; }
        private int miktar { get; set; }
        private int fiyat { get; set; }
        public bool adminOnay { get; set; }
     


        public jsonUrunler(string ad, int miktar, int fiyat)
        {
            this.ad = ad;
            this.miktar = miktar;
            this.fiyat = fiyat;
            
        }

        
        #region Urun ekleme

        public static void urunEkle(urunler u)
        {
            //urunler yeniUrun = new urunler(urun, miktar, fiyat);
            if (dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault().urun != null)
            {
                dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault()
                    .urun.Add(u);
                
            }
            else
            {
                dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault().urun = new List<urunler>();
                dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault()
                    .urun.Add(u);
                
            }

        }
        #endregion

       
    }
}
