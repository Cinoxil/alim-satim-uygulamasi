﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using yazilimYapimiProje.PanelIslem;
using System.Xml;
using System;

namespace yazilimYapimiProje.Entities
{
    public class paraIstek
    {
        private static int sayac = 1; //Paraistekid tutmak için oluşturulan sayaç fieldi.

        internal bool eklemeGecerlilik = true; //Şartları kontrol eden eklemeGecerlilik fieldi.

        //Para değerini alıp ilgili fieldlere eşitlemelrini yapıyor.
        public paraIstek(int para, string paraBirimi)
        {
            if (para != 0) //Eğer girilen para ideğeri 0 değil ise ekleme istek işlemini yapıyor.
            {
                paraMiktar = para;
                paraBirim = paraBirimi;
                adminOnay = false;
                paraId = sayac;

                if (
                    eklemeGecerlilik) //Ekleme geçerlilik true ise paraIstekOluştur methoduna o anki para istek nesnesi gönderilir.
                    paraIstekOlustur(this);
            }
        }

        private int _paraId { get; set; }
        private double _paraMiktar { get; set; }
        public bool adminOnay { get; set; } //Admin onayını kontrol için oluşturulan adminOnay fieldi.
        private string _paraBirim { get; set; } //paraIstegin hangi doviz tipinde oldugunu tutan field

        public int paraId
        {
            get => _paraId;
            set //Sayacın o ankideğerini paraidye eşitleyip sayacı bir artırıyor. 
            {
                _paraId = value;
                sayac++;
            }
        }

        public double paraMiktar
        {
            get => _paraMiktar;
            set //Girilen para sıfır veya sıfırdan küçükse hata verdirip ekleme geçerliliği false yapıyor.
            {
                if (value <= 0)
                {
                    MessageBox.Show("Lütfen geçerli bir değer giriniz");
                    eklemeGecerlilik = false;
                }
                else
                {
                    _paraMiktar = value;
                }
            }
        }

        public string paraBirim
        {
            get => _paraBirim;
            set
            {
                if (value != null)
                {
                    _paraBirim = value; //paraBirim null degilse ekler
                }
                else
                {
                    MessageBox.Show("Lütfen geçerli bir değer giriniz"); //null ise hata verip eklemeyi durdurur
                    eklemeGecerlilik = false;
                }
            }
        }
        #region Para ekleme istegi olusturma

        public static void paraIstekOlustur(paraIstek p)
        {
            //Para ekleme isteği oluşruran kullanıcı where ile bulup para istek listesi null değilse bu ife giriyor.
            if (dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault()
                .paraIstek != null)
            {
                dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault()
                    .paraIstek.Add(p); //Geçerli kullanıcının paraIstek listesine p nesnesini ekliyor.
                MessageBox.Show("Para ekleme isteği alındı");
                panelIslemleri.formTemizle(Application.OpenForms["paraEkle"]); //Para ekleme formunu temizliyor.
            }
            //Para istek listesi null ise buraya geliyor.
            else
            {
                //paraIstek listesi boş olduğu için listeyi önce örnekliyor.
                dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault()
                    .paraIstek = new List<paraIstek>();
                dataBase.dataBase.kullanicilar.Where(x => x.id == dataBase.dataBase.gecerliKullanici).FirstOrDefault()
                    .paraIstek.Add(p); //Geçerli kullanıcının paraIstek listesine p nesnesini ekliyor.
                MessageBox.Show("Para ekleme isteği alındı");
                panelIslemleri.formTemizle(Application.OpenForms["paraEkle"]); //Para ekleme formunu temizliyor.
            }
        }

        #endregion


        #region doviz islem

        public static Array dovizIslem() //admin ekraninda kolayca kullanabilmek icin oraya array tipinde donuyoruz
        {
            double[] dovizDizi = new double[3]; //dovizleri tutmak icin 3 elemanli bir double array yarattik

            const string bugun = "https://www.tcmb.gov.tr/kurlar/today.xml"; //xml'e cekmek icin bugun degiskeninde tcmb kuralar xml sayfasinin adresini tuttuk

            var cxml = new XmlDocument();
            cxml.Load(bugun); //bugunu xml'e yukluyoruz

            //@Kod girdimiz para birimini xmlde bulup gecerli degiskene esitliyor
            string usd_satis = cxml.SelectSingleNode("Tarih_Date/Currency [@Kod = 'USD']/BanknoteSelling").InnerXml;
            string eur_satis = cxml.SelectSingleNode("Tarih_Date/Currency [@Kod = 'EUR']/BanknoteSelling").InnerXml;
            string gbp_satis = cxml.SelectSingleNode("Tarih_Date/Currency [@Kod = 'GBP']/BanknoteSelling").InnerXml;

            //array'e usd, euro ve pound'u ekliyoruz
            dovizDizi[0] = double.Parse(usd_satis, System.Globalization.CultureInfo.InvariantCulture);
            dovizDizi[1] = double.Parse(eur_satis, System.Globalization.CultureInfo.InvariantCulture);
            dovizDizi[2] = double.Parse(gbp_satis, System.Globalization.CultureInfo.InvariantCulture);

            return dovizDizi;
        }
        #endregion

    }
}