﻿using System.Linq;
using System.Windows.Forms;

namespace yazilimYapimiProje.Entities
{
    public class admin
    {
        public string id { get; set; }
        public string sifre { get; set; }
        public string kullaniciTip { get; set; }
        public double komisyonPara { get; set; }

        //Bu fonksiyon kullanıcı idsini ve ürünid alıp ürünün isteğinin onaylanmasını sağlıyor.
        public void adminUrunOnay(string id, int urunId)
        {
            dataBase.dataBase.kullanicilar.Where(x => x.id == id).FirstOrDefault() //İdsi gelen kullacıyı where ile bulur.
                    .urun.Where(u => u.urunId == urunId).FirstOrDefault().adminOnay = true; //Bulunan kullacı içinde idsine göre ilgili ürünü bulup
                                                                                             //ürün içindeki admin onay fieldini true yapıyor.*/
            MessageBox.Show("Ürün ekleme isteği onaylandı");
        }

        //Bu fonksiyon kullanıcı idsini ve paraid alıp para isteğinin onaylanmasını sağlıyor.
        public void adminParaOnay(string id, int paraIstekId,string paraBirimi)
        {
            var k = dataBase.dataBase.kullanicilar.Where(x => x.id == id)
                .FirstOrDefault(); //kullanıcı tipinde k adında bir değişken oluşturup bu değişkene
                                   //where şartını sağlayan kullanıcıyı eşitledik

            if (k != null)
            {
                var istek = k.paraIstek.Where(p => p.paraId == paraIstekId)
                    .FirstOrDefault(); //paraIstek tipinde istek adında bir değişken oluşturup
                                       //bu değişkene where şartını sağlayan para isteğini eşitledik

                double[] dovizler = new double[3];
                dovizler = (double[])paraIstek.dovizIslem(); //paraIstek classindaki dovizIslem fonksiyonundan
                                                             //double array'e doviz kurlarini cekiyoruz

                istek.adminOnay = true; //admin para ekleme istegini onayladiysa
                                        //istek icindeki admin onay fieldini true yapiyoruz

                if (paraBirimi == "TL")
                {
                    k.para += istek.paraMiktar; //kullanicinin parasina tip tl oldugu icin paraMiktari direk ekledik
                }
                else if(paraBirimi == "USD")
                {
                    k.para += ((istek.paraMiktar) * (dovizler[0])); //kullanicinin parasina tip usd oldugu
                                                                    //icin paraMiktari o anki kurla carpip ekledik
                }
                else if (paraBirimi == "EUR")
                {
                    k.para += ((istek.paraMiktar) * (dovizler[1]));
                }
                else if (paraBirimi == "GBP")
                {
                    k.para += ((istek.paraMiktar) * (dovizler[2]));
                }

            }

            MessageBox.Show("Para ekleme isteği onaylandı");
        }
    }
}