﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using yazilimYapimiProje.PanelIslem;

namespace yazilimYapimiProje.Entities
{
    public class kullanici : admin
    {
        private string _tc;
        public bool kayitAlinabilir = true; //Kayıt şartlarına uyup uyulamdığını test etmek için oluşturulan field.

        //Üyelik formundan kullanıcı bilgilerini alıyor ilgili fieldlere eşitliyor.
        public kullanici(string id, string sifre, string ad, string soyad, string tc, string telefon, string mail,
            string adres)
        {
            this.id = id;
            this.sifre = sifre;
            kullaniciTip = "kullanici"; //Default kullanıcı tipinde olarak alıyor.
            this.ad = ad;
            this.soyad = soyad;
            this.tc = tc;
            this.telefon = telefon;
            this.mail = mail;
            this.adres = adres;
        }

        public string ad { get; set; }
        public string soyad { get; set; }
        public string _telefon { get; set; }
        public string mail { get; set; }
        public string adres { get; set; }
        public List<urunler> urun { get; set; } //kullanıcının ürünlerini liste ürünler tipinde tutuyor.
        public List<urunler> satinAlinanUrunler { get; set; }
        public double para { get; set; }

        public List<paraIstek> paraIstek { get; set; } //kullanıcının para isteklerini liste paraIstek tipinde tutuyor.

        //Kullanıcıları kontrol edip kayıt olunmaya çalışılan mail ve kullanıcı adı bilgilerinin daha önce girilip girilmediğini kontrol eder.
        public bool kayitTekrarKontrol(kullanici k)
        {
            if (dataBase.dataBase.kullanicilar != null)
            {
                foreach (var kisi in dataBase.dataBase.kullanicilar) //Databasedaki kullanıcılar listesinde gezer
                {
                    if (k.id == kisi.id || k.mail == kisi.mail) //Eğer kullanıcı adı veya mail daha önce girilmişse kayitalınabiliri false yapar.
                    {
                        MessageBox.Show("Bu kişi daha önce kayıt olmuş");
                        kayitAlinabilir = false;
                        break;
                    }

                }

            }
            return kayitAlinabilir;
        }

        #region kullanici giriş kontrol

        //Girilen kullanıcı adı ve şifre değerlerini var olan kullanıcı adı ve şifre değerleriyle kıyaslayıp doğru ise giriş işlemini gerçekleştirir.
        public static void kullaniciGirisKontrol(string kullaniciAdi, string sifre, Form frmKullaniciGiris)
        {
            var kullaniciKontrol = false;
            //Database içindeki adminler listesinde gezip fonksiyona gelen kullanıcı adında admin olup olmadığını kuntrol ediyor. 
            var adminTemp = dataBase.dataBase.adminler.Where(x => x.id == kullaniciAdi).FirstOrDefault();
            if (adminTemp != null) //AdminTempin null olup olmadığına bakıyor.
            {
                if (kullaniciAdi != null && kullaniciAdi == adminTemp.id &&
                    sifre == adminTemp.sifre) /*Gelen değerlerle adminTempdeki değerler eşit mi diye kontrol ediyor.
                                                                               Eşitse admin girişini yapıyor.*/
                {
                    MessageBox.Show("Merhaba " + adminTemp.id);
                    frmKullaniciGiris.Close(); //Fonsiyona gelen geçerli giriş yapma formunu kapatıyor.
                    dataBase.dataBase.gecerliKullanici =
                        adminTemp.id; //Geçerli kullanıcıya adminTempdeki idyi gönderir.
                    panelIslemleri.kullaniciyaEkraniGoster(adminTemp
                        .kullaniciTip); //Kullanıcı tipine göre gerekli ekranlari ayarlar.
                }
                else
                {
                    MessageBox.Show("Şifreniz yanlış",
                        "Hata", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else //Eğer adminler listesinde bu kullanıcı adına sahip biri bulumazsa kullanıcı konrtolü true yapar.
            {
                kullaniciKontrol = true;
            }

            if (kullaniciKontrol)
            {
                if (dataBase.dataBase.kullanicilar != null)
                {
                    //kullanıcıTempe girilen kulanıcıadına sahip olan kullanıcı eşitliyoruz.
                    var kullaniciTemp = dataBase.dataBase.kullanicilar.Where(x => x.id == kullaniciAdi)
                        .FirstOrDefault();
                    if (kullaniciTemp != null) //Eğer kullanıcı bulunursa bu şarta girer.
                    {
                        //Kullanıcı kontrollerini ve form işlemlerini yapaıyor.
                        if (kullaniciAdi != null && kullaniciAdi == kullaniciTemp.id && sifre == kullaniciTemp.sifre)
                        {
                            MessageBox.Show("Merhaba " + kullaniciTemp.id);
                            frmKullaniciGiris.Close(); //Fonsiyona gelen geçerli giriş yapma formunu kapatıyor.
                            dataBase.dataBase.gecerliKullanici =
                                kullaniciTemp.id; //Geçerli kullanıcıya adminTempdeki idyi gönderir.
                            panelIslemleri.kullaniciyaEkraniGoster(kullaniciTemp
                                .kullaniciTip); /*Kullanıcı tipine göre gerekli ekranlari
                                                                                                            ayarlar.*/
                        }
                        else
                        {
                            MessageBox.Show("Şifreniz yanlış",
                                "Hata", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Böyle bir kayıt bulunamadı",
                            "Hata", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Böyle bir kayıt bulunamadı",
                        "Hata", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        #endregion

        #region Properties

        //Kayıt ekranından gelen telefon bilgisini alıp ilgili kontrollari yapıp ekleme işlemini gerçekleştiriyor.
        public string telefon
        {
            get => _telefon;
            set
            {
                var gsm = value.ToCharArray(); //Gelen değeri char array tipinde gsm değişkenine eşitliyor.
                var gsmNo = value;
                var durum = false;

                for (var i = 0; i < value.Length; i++)
                    if (
                        !char.IsDigit(
                            gsm[i])) //Gelen değer sayı mı diye kontrol ediyor, eğer değilse durum değişkenini true yapıyor.
                    {
                        durum = true;
                        break;
                    }

                //Eğer durum true ise var olan gelen numarayı sadece sayı olacak şekilde düzenliyor(masked texbox kullanıldığı için).
                if (durum)
                {
                    for (var i = 0; i < value.Length; i++)
                        if (!char.IsDigit(gsm[i])) //gsm dizisin içindeki sayı olamayan karakterleri siliyor.
                        {
                            gsmNo = value.Remove(i, 1); //Valuenin i. değerini siler.
                            value = gsmNo; //Valueye for içinde indeks kaymasın diye tekrar gsmNoyu eşitler.
                            gsm = value.ToCharArray(); //değişen valueyi gsme eşitler.
                        }

                    gsmNo = value.Remove(3,
                        1); //Masked texboxdan dolayı üçüncü indis boşluk oluyor ve o boşluğu siliyor.
                }

                //Eğer Telefon numarası 10 hanenin altında değil ise kayıt alınabiliri true yapar.
                if (gsmNo.Length < 10)
                {
                    MessageBox.Show("Telefon numarası 10 haneli olmalıdır");
                    kayitAlinabilir = false;
                }
                else
                {
                    _telefon = gsmNo;
                }
            }
        }

        //Eğer TC 11 hanenin altında değil ise kayıt alınabiliri true yapar.
        public string tc
        {
            get => _tc;
            set
            {
                if (value.Length < 11) //Eğer valuenin uzunluğu 11den küçükse kayıt alınabiliri false yapar.
                {
                    MessageBox.Show("T.C. Kimlik numaranız 11 haneli olmalıdır!");
                    kayitAlinabilir = false;
                }
                else
                {
                    _tc = value;
                }
            }
        }

        #endregion

        #region kullanıcı db aktar

        //admin tipindeki nesneyi database içindeki adminler listesine ekler.
        public static void kullaniciEkle(admin a)
        {
            dataBase.dataBase.adminler.Add(a);
        }

        //kullanıcı tipindeki nesneyi database içindeki kullanıcılar listesine ekler.
        public static void kullaniciEkle(kullanici a)
        {
            if (dataBase.dataBase.kullanicilar != null)
            {
                dataBase.dataBase.kullanicilar.Add(a);
            }
            else //Eğer databasedeki kullanıcılar listesi boş ise listeyi önce örnekler daha sonra ekleme işlemini yapar.
            {
                dataBase.dataBase.kullanicilar = new List<kullanici>();
                dataBase.dataBase.kullanicilar.Add(a);
            }
        }

        #endregion
    }
}