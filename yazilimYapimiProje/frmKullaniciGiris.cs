﻿using System;
using System.Windows.Forms;
using yazilimYapimiProje.Entities;
using yazilimYapimiProje.PanelIslem;

namespace yazilimYapimiProje
{
    public partial class frmKullaniciGiris : Form
    {
        public frmKullaniciGiris()
        {
            InitializeComponent();
        }

        private void btnGiris_Click(object sender, EventArgs e)
        {
            //Texboxlardan bilgiler alınıp kullanıcı classı altındaki kullaniciGirisKontrol fonsiyonuna gönderiliyor.
            kullanici.kullaniciGirisKontrol(txtKullaniciAdi.Text, txtSifre.Text,
                Application.OpenForms["frmKullaniciGiris"]);
        }

        private void btnKayit_Click(object sender, EventArgs e)
        {
            panelIslemleri.kayitOlPencereGoster();
        }
    }
}