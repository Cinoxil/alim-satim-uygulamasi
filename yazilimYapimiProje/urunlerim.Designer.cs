﻿
namespace yazilimYapimiProje
{
    partial class urunlerim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgUrunlerim = new System.Windows.Forms.DataGridView();
            this.dtpBaslangic = new System.Windows.Forms.DateTimePicker();
            this.dtpBitis = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAra = new System.Windows.Forms.Button();
            this.btnExcell = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgUrunlerim)).BeginInit();
            this.SuspendLayout();
            // 
            // dgUrunlerim
            // 
            this.dgUrunlerim.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUrunlerim.Location = new System.Drawing.Point(12, 58);
            this.dgUrunlerim.Name = "dgUrunlerim";
            this.dgUrunlerim.RowHeadersWidth = 51;
            this.dgUrunlerim.Size = new System.Drawing.Size(460, 278);
            this.dgUrunlerim.TabIndex = 0;
            // 
            // dtpBaslangic
            // 
            this.dtpBaslangic.Location = new System.Drawing.Point(100, 2);
            this.dtpBaslangic.MaxDate = new System.DateTime(2025, 12, 25, 0, 0, 0, 0);
            this.dtpBaslangic.MinDate = new System.DateTime(2000, 12, 25, 0, 0, 0, 0);
            this.dtpBaslangic.Name = "dtpBaslangic";
            this.dtpBaslangic.Size = new System.Drawing.Size(200, 20);
            this.dtpBaslangic.TabIndex = 1;
            this.dtpBaslangic.Value = new System.DateTime(2021, 6, 24, 0, 0, 0, 0);
            // 
            // dtpBitis
            // 
            this.dtpBitis.Location = new System.Drawing.Point(99, 28);
            this.dtpBitis.MaxDate = new System.DateTime(2025, 12, 25, 0, 0, 0, 0);
            this.dtpBitis.MinDate = new System.DateTime(2000, 12, 25, 0, 0, 0, 0);
            this.dtpBitis.Name = "dtpBitis";
            this.dtpBitis.Size = new System.Drawing.Size(200, 20);
            this.dtpBitis.TabIndex = 1;
            this.dtpBitis.Value = new System.DateTime(2021, 6, 12, 23, 59, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Baslangic :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Bitis :";
            // 
            // btnAra
            // 
            this.btnAra.Location = new System.Drawing.Point(305, 2);
            this.btnAra.Name = "btnAra";
            this.btnAra.Size = new System.Drawing.Size(55, 49);
            this.btnAra.TabIndex = 3;
            this.btnAra.Text = "Ara";
            this.btnAra.UseVisualStyleBackColor = true;
            this.btnAra.Click += new System.EventHandler(this.btnAra_Click);
            // 
            // btnExcell
            // 
            this.btnExcell.Location = new System.Drawing.Point(362, 342);
            this.btnExcell.Name = "btnExcell";
            this.btnExcell.Size = new System.Drawing.Size(110, 49);
            this.btnExcell.TabIndex = 3;
            this.btnExcell.Text = "Excell\'e Aktar";
            this.btnExcell.UseVisualStyleBackColor = true;
            this.btnExcell.Click += new System.EventHandler(this.btnRapor_Click);
            // 
            // urunlerim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 397);
            this.Controls.Add(this.btnExcell);
            this.Controls.Add(this.btnAra);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpBitis);
            this.Controls.Add(this.dtpBaslangic);
            this.Controls.Add(this.dgUrunlerim);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "urunlerim";
            this.Text = "urunlerim";
            this.Load += new System.EventHandler(this.urunlerim_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgUrunlerim)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgUrunlerim;
        private System.Windows.Forms.DateTimePicker dtpBaslangic;
        private System.Windows.Forms.DateTimePicker dtpBitis;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAra;
        private System.Windows.Forms.Button btnExcell;
    }
}