﻿
namespace yazilimYapimiProje
{
    partial class urunEkle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(urunEkle));
            this.cmbUrun = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMiktar = new System.Windows.Forms.TextBox();
            this.pbUnlem = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFiyat = new System.Windows.Forms.TextBox();
            this.btnEkle = new System.Windows.Forms.Button();
            this.gbUrunTxt = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbUnlem)).BeginInit();
            this.gbUrunTxt.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbUrun
            // 
            this.cmbUrun.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUrun.FormattingEnabled = true;
            this.cmbUrun.Items.AddRange(new object[] {
            "Buğday",
            "Arpa",
            "Yulaf",
            "Pamuk",
            "Patates"});
            this.cmbUrun.Location = new System.Drawing.Point(103, 25);
            this.cmbUrun.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbUrun.Name = "cmbUrun";
            this.cmbUrun.Size = new System.Drawing.Size(160, 24);
            this.cmbUrun.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ürün";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 82);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Miktar";
            // 
            // txtMiktar
            // 
            this.txtMiktar.Location = new System.Drawing.Point(103, 79);
            this.txtMiktar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMiktar.Name = "txtMiktar";
            this.txtMiktar.Size = new System.Drawing.Size(160, 22);
            this.txtMiktar.TabIndex = 1;
            // 
            // pbUnlem
            // 
            this.pbUnlem.Image = ((System.Drawing.Image)(resources.GetObject("pbUnlem.Image")));
            this.pbUnlem.Location = new System.Drawing.Point(272, 79);
            this.pbUnlem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbUnlem.Name = "pbUnlem";
            this.pbUnlem.Size = new System.Drawing.Size(28, 25);
            this.pbUnlem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUnlem.TabIndex = 3;
            this.pbUnlem.TabStop = false;
            this.pbUnlem.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 137);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Birim Fiyat";
            // 
            // txtFiyat
            // 
            this.txtFiyat.Location = new System.Drawing.Point(103, 133);
            this.txtFiyat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFiyat.Name = "txtFiyat";
            this.txtFiyat.Size = new System.Drawing.Size(160, 22);
            this.txtFiyat.TabIndex = 2;
            // 
            // btnEkle
            // 
            this.btnEkle.Location = new System.Drawing.Point(103, 169);
            this.btnEkle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEkle.Name = "btnEkle";
            this.btnEkle.Size = new System.Drawing.Size(100, 28);
            this.btnEkle.TabIndex = 3;
            this.btnEkle.Text = "Ekle";
            this.btnEkle.UseVisualStyleBackColor = true;
            this.btnEkle.Click += new System.EventHandler(this.btnEkle_Click);
            // 
            // gbUrunTxt
            // 
            this.gbUrunTxt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gbUrunTxt.Controls.Add(this.cmbUrun);
            this.gbUrunTxt.Controls.Add(this.pbUnlem);
            this.gbUrunTxt.Controls.Add(this.btnEkle);
            this.gbUrunTxt.Controls.Add(this.txtMiktar);
            this.gbUrunTxt.Controls.Add(this.txtFiyat);
            this.gbUrunTxt.Controls.Add(this.label3);
            this.gbUrunTxt.Controls.Add(this.label1);
            this.gbUrunTxt.Controls.Add(this.label2);
            this.gbUrunTxt.Location = new System.Drawing.Point(-4, -12);
            this.gbUrunTxt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUrunTxt.Name = "gbUrunTxt";
            this.gbUrunTxt.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUrunTxt.Size = new System.Drawing.Size(321, 244);
            this.gbUrunTxt.TabIndex = 5;
            this.gbUrunTxt.TabStop = false;
            // 
            // urunEkle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 199);
            this.Controls.Add(this.gbUrunTxt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "urunEkle";
            this.Text = "urunEkle";
            ((System.ComponentModel.ISupportInitialize)(this.pbUnlem)).EndInit();
            this.gbUrunTxt.ResumeLayout(false);
            this.gbUrunTxt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbUrun;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMiktar;
        private System.Windows.Forms.PictureBox pbUnlem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFiyat;
        private System.Windows.Forms.Button btnEkle;
        private System.Windows.Forms.GroupBox gbUrunTxt;
    }
}