﻿
namespace yazilimYapimiProje
{
    partial class form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form1));
            this.grpBoxIslemListe = new System.Windows.Forms.GroupBox();
            this.btnParaEkle = new System.Windows.Forms.Button();
            this.btnUrunEkle = new System.Windows.Forms.Button();
            this.btnOturumKapat = new System.Windows.Forms.Button();
            this.btnUrunlerim = new System.Windows.Forms.Button();
            this.btnSatinAl = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlIslemler = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPara = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblUsd = new System.Windows.Forms.Label();
            this.lblEur = new System.Windows.Forms.Label();
            this.lblGbp = new System.Windows.Forms.Label();
            this.grpBoxIslemListe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlIslemler.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBoxIslemListe
            // 
            this.grpBoxIslemListe.Controls.Add(this.btnParaEkle);
            this.grpBoxIslemListe.Controls.Add(this.btnUrunEkle);
            this.grpBoxIslemListe.Controls.Add(this.btnOturumKapat);
            this.grpBoxIslemListe.Controls.Add(this.btnUrunlerim);
            this.grpBoxIslemListe.Controls.Add(this.btnSatinAl);
            this.grpBoxIslemListe.Location = new System.Drawing.Point(3, 104);
            this.grpBoxIslemListe.Name = "grpBoxIslemListe";
            this.grpBoxIslemListe.Size = new System.Drawing.Size(115, 206);
            this.grpBoxIslemListe.TabIndex = 1;
            this.grpBoxIslemListe.TabStop = false;
            this.grpBoxIslemListe.Text = "İşlem Listesi";
            // 
            // btnParaEkle
            // 
            this.btnParaEkle.Enabled = false;
            this.btnParaEkle.Location = new System.Drawing.Point(6, 55);
            this.btnParaEkle.Name = "btnParaEkle";
            this.btnParaEkle.Size = new System.Drawing.Size(102, 28);
            this.btnParaEkle.TabIndex = 1;
            this.btnParaEkle.Text = "Para Ekle";
            this.btnParaEkle.UseVisualStyleBackColor = true;
            this.btnParaEkle.Click += new System.EventHandler(this.btnParaEkle_Click);
            // 
            // btnUrunEkle
            // 
            this.btnUrunEkle.Enabled = false;
            this.btnUrunEkle.Location = new System.Drawing.Point(6, 21);
            this.btnUrunEkle.Name = "btnUrunEkle";
            this.btnUrunEkle.Size = new System.Drawing.Size(102, 28);
            this.btnUrunEkle.TabIndex = 0;
            this.btnUrunEkle.Text = "Ürün Ekle";
            this.btnUrunEkle.UseVisualStyleBackColor = true;
            this.btnUrunEkle.Click += new System.EventHandler(this.btnUrunEkle_Click);
            // 
            // btnOturumKapat
            // 
            this.btnOturumKapat.Enabled = false;
            this.btnOturumKapat.Location = new System.Drawing.Point(7, 157);
            this.btnOturumKapat.Name = "btnOturumKapat";
            this.btnOturumKapat.Size = new System.Drawing.Size(102, 28);
            this.btnOturumKapat.TabIndex = 4;
            this.btnOturumKapat.Text = "Oturumu Kapat";
            this.btnOturumKapat.UseVisualStyleBackColor = true;
            this.btnOturumKapat.Click += new System.EventHandler(this.btnOturumKapat_Click);
            // 
            // btnUrunlerim
            // 
            this.btnUrunlerim.Enabled = false;
            this.btnUrunlerim.Location = new System.Drawing.Point(6, 123);
            this.btnUrunlerim.Name = "btnUrunlerim";
            this.btnUrunlerim.Size = new System.Drawing.Size(102, 28);
            this.btnUrunlerim.TabIndex = 3;
            this.btnUrunlerim.Text = "Ürünlerim";
            this.btnUrunlerim.UseVisualStyleBackColor = true;
            this.btnUrunlerim.Click += new System.EventHandler(this.btnUrunlerim_Click);
            // 
            // btnSatinAl
            // 
            this.btnSatinAl.Enabled = false;
            this.btnSatinAl.Location = new System.Drawing.Point(7, 89);
            this.btnSatinAl.Name = "btnSatinAl";
            this.btnSatinAl.Size = new System.Drawing.Size(102, 28);
            this.btnSatinAl.TabIndex = 2;
            this.btnSatinAl.Text = "Satın Al";
            this.btnSatinAl.UseVisualStyleBackColor = true;
            this.btnSatinAl.Click += new System.EventHandler(this.btnSatinAl_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(115, 95);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pnlIslemler
            // 
            this.pnlIslemler.Controls.Add(this.lblGbp);
            this.pnlIslemler.Controls.Add(this.label4);
            this.pnlIslemler.Controls.Add(this.lblEur);
            this.pnlIslemler.Controls.Add(this.label3);
            this.pnlIslemler.Controls.Add(this.lblUsd);
            this.pnlIslemler.Controls.Add(this.label2);
            this.pnlIslemler.Controls.Add(this.label1);
            this.pnlIslemler.Controls.Add(this.lblPara);
            this.pnlIslemler.Controls.Add(this.lblId);
            this.pnlIslemler.Controls.Add(this.pictureBox1);
            this.pnlIslemler.Controls.Add(this.grpBoxIslemListe);
            this.pnlIslemler.Location = new System.Drawing.Point(1, 0);
            this.pnlIslemler.Name = "pnlIslemler";
            this.pnlIslemler.Size = new System.Drawing.Size(124, 449);
            this.pnlIslemler.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 428);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Para :";
            // 
            // lblPara
            // 
            this.lblPara.AutoSize = true;
            this.lblPara.Location = new System.Drawing.Point(48, 428);
            this.lblPara.Name = "lblPara";
            this.lblPara.Size = new System.Drawing.Size(28, 13);
            this.lblPara.TabIndex = 2;
            this.lblPara.Text = "para";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(11, 406);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(15, 13);
            this.lblId.TabIndex = 2;
            this.lblId.Text = "id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 317);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "USD :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 341);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "EUR :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 366);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "GBP :";
            // 
            // lblUsd
            // 
            this.lblUsd.AutoSize = true;
            this.lblUsd.Location = new System.Drawing.Point(50, 317);
            this.lblUsd.Name = "lblUsd";
            this.lblUsd.Size = new System.Drawing.Size(35, 13);
            this.lblUsd.TabIndex = 3;
            this.lblUsd.Text = "label2";
            // 
            // lblEur
            // 
            this.lblEur.AutoSize = true;
            this.lblEur.Location = new System.Drawing.Point(48, 341);
            this.lblEur.Name = "lblEur";
            this.lblEur.Size = new System.Drawing.Size(35, 13);
            this.lblEur.TabIndex = 3;
            this.lblEur.Text = "label2";
            // 
            // lblGbp
            // 
            this.lblGbp.AutoSize = true;
            this.lblGbp.Location = new System.Drawing.Point(48, 366);
            this.lblGbp.Name = "lblGbp";
            this.lblGbp.Size = new System.Drawing.Size(35, 13);
            this.lblGbp.TabIndex = 3;
            this.lblGbp.Text = "label2";
            // 
            // form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pnlIslemler);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.IsMdiContainer = true;
            this.Name = "form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alım - Satım Uygulaması";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpBoxIslemListe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlIslemler.ResumeLayout(false);
            this.pnlIslemler.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBoxIslemListe;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnParaEkle;
        private System.Windows.Forms.Button btnUrunEkle;
        private System.Windows.Forms.Button btnSatinAl;
        private System.Windows.Forms.Panel pnlIslemler;
        private System.Windows.Forms.Button btnOturumKapat;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPara;
        private System.Windows.Forms.Button btnUrunlerim;
        private System.Windows.Forms.Label lblGbp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblEur;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUsd;
        private System.Windows.Forms.Label label2;
    }
}

