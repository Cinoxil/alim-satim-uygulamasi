﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using yazilimYapimiProje.Entities;
using yazilimYapimiProje.PanelIslem;

namespace yazilimYapimiProje
{
    public partial class frmAna : Form
    {
        public frmAna()
        {
            //frmAna kapatıldığında anaFormKapatma fonksiyonunu çalıştırıyor.
            FormClosing += anaFormKapatma;
            InitializeComponent();
        }

        //Anaform kapatıldığında ile database içindeki serializeJSON methodunu çalıştırıyor.
        private void anaFormKapatma(object sender, FormClosingEventArgs e)
        {
            dataBase.dataBase.serializeJSON();
        }

        private void frmAna_Load(object sender, EventArgs e)
        {
            dataBase.dataBase
                .deSerializeJSON(); //Program ilk açıldığında database içindeki deSerializeJSON methodu çalışıyor.
            var kullaniciKontrol = new frmKullaniciGiris();
            kullaniciKontrol.MdiParent = this; //frmAnai frmKullanıcıGiris formunun MdiParrentı olarak belirliyor.
            kullaniciKontrol.StartPosition = FormStartPosition.Manual;
            kullaniciKontrol.Location = new Point(280, 130);
            kullaniciKontrol.Show();
            //bir adet default admin hesabı oluşturuyor.
            if (dataBase.dataBase.adminler == null)
            {
                dataBase.dataBase.adminler = new List<admin>();
                var a = new admin();
                a.id = "admin";
                a.sifre = "123";
                a.kullaniciTip = "admin";
                kullanici.kullaniciEkle(a); //a nesnesini kullanıcı sınıfındaki kullanıcı ekle fonksiyonuna yolluyor.
            }

            double[] dovizler = new double[3];
            dovizler = (double[])paraIstek.dovizIslem();

            lblUsd.Text = dovizler[0].ToString();
            lblEur.Text = dovizler[1].ToString();
            lblGbp.Text = dovizler[2].ToString();


        }

        #region buton işlemleri

        private void btnOturumKapat_Click(object sender, EventArgs e)
        {
            dataBase.dataBase
                .serializeJSON(); //btnOturumKapat butununa tıklandığında database içindeki deSerializeJSON methodu çalışıyor.
            panelIslemleri.pencereleriKapat();
            panelIslemleri.oturumuKapat();
        }

        private void btnUrunEkle_Click(object sender, EventArgs e)
        {
            var frmEkle = new urunEkle();
            frmEkle.MdiParent = this; //frmAnai frmEkle formunun MdiParrentı olarak belirliyor.
            frmEkle.StartPosition = FormStartPosition.Manual;
            frmEkle.Location = new Point(330, 120);
            panelIslemleri.pencereleriKapat();
            frmEkle.Show();
        }

        private void btnParaEkle_Click(object sender, EventArgs e)
        {
            var paraEkle = new paraEkle();
            paraEkle.MdiParent = this; //frmAnai paraEkle formunun MdiParrentı olarak belirliyor.
            paraEkle.StartPosition = FormStartPosition.Manual;
            paraEkle.Location = new Point(300, 160);
            panelIslemleri.pencereleriKapat();
            paraEkle.Show();
        }

        private void btnSatinAl_Click(object sender, EventArgs e)
        {
            var satinAl = new satinAl();
            satinAl.MdiParent = this; //frmAnai satinAl formunun MdiParrentı olarak belirliyor.
            satinAl.StartPosition = FormStartPosition.Manual;
            satinAl.Location = new Point(125, 80);
            panelIslemleri.pencereleriKapat();
            satinAl.Show();
        }

        private void btnUrunlerim_Click(object sender, EventArgs e)
        {
            var urunlerim = new urunlerim();
            urunlerim.MdiParent = this; //frmAnai urunlerim formunun MdiParrentı olarak belirliyor.
            urunlerim.StartPosition = FormStartPosition.Manual;
            urunlerim.Location = new Point(221, 24);
            panelIslemleri.pencereleriKapat();
            urunlerim.Show();
        }

        #endregion
    }
}