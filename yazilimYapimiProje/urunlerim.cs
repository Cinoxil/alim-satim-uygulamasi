﻿using System;
using System.Windows.Forms;
using yazilimYapimiProje.Entities;
using yazilimYapimiProje.PanelIslem;

namespace yazilimYapimiProje
{
    public partial class urunlerim : Form
    {
        public urunlerim()
        {
            InitializeComponent();
        }

        private void urunlerim_Load(object sender, EventArgs e)
        {
            dgUrunlerim.AllowUserToDeleteRows = false;
            //Datagridwievın sütün sayısını ve satır yazılarını oluşturuyor.
            dgUrunlerim.ColumnCount = 4;
            dgUrunlerim.Columns[0].Name = "Ürün";
            dgUrunlerim.Columns[1].Name = "Miktar";
            dgUrunlerim.Columns[2].Name = "Fiyat";
            dgUrunlerim.Columns[3].Name = "Alinma Tarihi";

            urunler.urunleriListele(dgUrunlerim, dataBase.dataBase.gecerliKullanici);
        }

        private void btnAra_Click(object sender, EventArgs e)
        {
            dgUrunlerim.Rows.Clear(); //dataGridView'un satirlarini temizledik
            urunler.urunleriListele(dgUrunlerim, dataBase.dataBase.gecerliKullanici,dtpBaslangic.Value,dtpBitis.Value);//baslangic ve bitis tarihine gore urunleri listeliyor
        }

        private void btnRapor_Click(object sender, EventArgs e)
        {
            raporAl(); //raporAl fonksiyonunu cagirdik
        }

        private void raporAl()
        {
            SaveFileDialog save = new SaveFileDialog(); //SaveFileDialog tipinde save nesnesi olusturduk
            save.OverwritePrompt = false; 
            save.Title = "Excel Dosyaları"; //dosya title'i Excel Dosyaları olarak ayarliyor
            save.DefaultExt = "xlsx"; //default uzanti xlsx
            save.Filter = "xlsx Dosyaları (*.xlsx)|*.xlsx|Tüm Dosyalar(*.*)|*.*";

            if (save.ShowDialog() == DialogResult.OK)
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application(); //Microsoft.Office.Interop.Excel._Application tipinde app
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing); // Microsoft.Office.Interop.Excel._Workbook tipinde workbook
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null; //Microsoft.Office.Interop.Excel._Worksheet worksheet nesleri yarattik
                app.Visible = true;
                worksheet = workbook.Sheets["Sayfa1"]; //worksheet adi sayfa1 yaptik
                worksheet = workbook.ActiveSheet; //sayfayi etkinlestirdik
                worksheet.Name = "Excel Dışa Aktarım"; //sayfanin adi Excel Dışa Aktarım
                worksheet.Cells[1, 1] = "Ürün adı"; //bu 4 satir en ustte yazan column isimleri
                worksheet.Cells[1, 2] = "Miktar";
                worksheet.Cells[1, 3] = "Fiyat";
                worksheet.Cells[1, 4] = "Tarih";

                for (int i = 1; i <= dgUrunlerim.RowCount; i++) //formdaki dayaGridView'un satir sayisi kadar geziyor
                {
                    for (int j = 0; j < dgUrunlerim.Columns.Count; j++) //formdaki dayaGridView'un sutun sayisi kadar geziyor
                    {
                        if (dgUrunlerim.Rows[i - 1].Cells[j].Value != null) //eger (i-1).satirin j.hucresi bos degilse girer
                        {
                            worksheet.Cells[i + 1, j + 1] = dgUrunlerim.Rows[i - 1].Cells[j].Value.ToString(); //exceldeki i+1'e j+1. hucreye
                                                                                                               //dataGridden gelen (i-1).satirin j.hucresini esitler
                        }
                    }
                }
                workbook.SaveAs(save.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                app.Quit();
            }
        }
    }
}