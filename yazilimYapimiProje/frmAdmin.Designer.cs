﻿
namespace yazilimYapimiProje
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbUrunEklemeIstek = new System.Windows.Forms.GroupBox();
            this.lblUrun = new System.Windows.Forms.Label();
            this.btnUrunReddet = new System.Windows.Forms.Button();
            this.btnUrunOnayla = new System.Windows.Forms.Button();
            this.dgUrun = new System.Windows.Forms.DataGridView();
            this.gbParaEklemeIstek = new System.Windows.Forms.GroupBox();
            this.lblPara = new System.Windows.Forms.Label();
            this.btnParaReddet = new System.Windows.Forms.Button();
            this.dgPara = new System.Windows.Forms.DataGridView();
            this.btnParaOnayla = new System.Windows.Forms.Button();
            this.btnOturumKapat = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_usd = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_eur = new System.Windows.Forms.Label();
            this.lbl_gbp = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblBakiye = new System.Windows.Forms.Label();
            this.dataBaseBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataBaseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.paraIstekBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.gbUrunEklemeIstek.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUrun)).BeginInit();
            this.gbParaEklemeIstek.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPara)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paraIstekBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gbUrunEklemeIstek
            // 
            this.gbUrunEklemeIstek.Controls.Add(this.lblUrun);
            this.gbUrunEklemeIstek.Controls.Add(this.btnUrunReddet);
            this.gbUrunEklemeIstek.Controls.Add(this.btnUrunOnayla);
            this.gbUrunEklemeIstek.Controls.Add(this.dgUrun);
            this.gbUrunEklemeIstek.Location = new System.Drawing.Point(12, 12);
            this.gbUrunEklemeIstek.Name = "gbUrunEklemeIstek";
            this.gbUrunEklemeIstek.Size = new System.Drawing.Size(593, 178);
            this.gbUrunEklemeIstek.TabIndex = 0;
            this.gbUrunEklemeIstek.TabStop = false;
            this.gbUrunEklemeIstek.Text = "Ürün Ekleme İstekleri";
            // 
            // lblUrun
            // 
            this.lblUrun.AutoSize = true;
            this.lblUrun.Location = new System.Drawing.Point(6, 126);
            this.lblUrun.Name = "lblUrun";
            this.lblUrun.Size = new System.Drawing.Size(35, 13);
            this.lblUrun.TabIndex = 5;
            this.lblUrun.Text = "label1";
            // 
            // btnUrunReddet
            // 
            this.btnUrunReddet.Location = new System.Drawing.Point(87, 149);
            this.btnUrunReddet.Name = "btnUrunReddet";
            this.btnUrunReddet.Size = new System.Drawing.Size(75, 23);
            this.btnUrunReddet.TabIndex = 2;
            this.btnUrunReddet.Text = "Reddet";
            this.btnUrunReddet.UseVisualStyleBackColor = true;
            this.btnUrunReddet.Click += new System.EventHandler(this.btnUrunReddet_Click);
            // 
            // btnUrunOnayla
            // 
            this.btnUrunOnayla.Location = new System.Drawing.Point(6, 149);
            this.btnUrunOnayla.Name = "btnUrunOnayla";
            this.btnUrunOnayla.Size = new System.Drawing.Size(75, 23);
            this.btnUrunOnayla.TabIndex = 1;
            this.btnUrunOnayla.Text = "Onayla";
            this.btnUrunOnayla.UseVisualStyleBackColor = true;
            this.btnUrunOnayla.Click += new System.EventHandler(this.btnUrunOnayla_Click);
            // 
            // dgUrun
            // 
            this.dgUrun.AllowUserToAddRows = false;
            this.dgUrun.AllowUserToDeleteRows = false;
            this.dgUrun.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUrun.Location = new System.Drawing.Point(6, 19);
            this.dgUrun.Name = "dgUrun";
            this.dgUrun.ReadOnly = true;
            this.dgUrun.RowHeadersWidth = 51;
            this.dgUrun.Size = new System.Drawing.Size(581, 95);
            this.dgUrun.TabIndex = 0;
            this.dgUrun.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUrun_CellClick);
            this.dgUrun.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgUrun_CellFormatting);
            // 
            // gbParaEklemeIstek
            // 
            this.gbParaEklemeIstek.Controls.Add(this.lblPara);
            this.gbParaEklemeIstek.Controls.Add(this.btnParaReddet);
            this.gbParaEklemeIstek.Controls.Add(this.dgPara);
            this.gbParaEklemeIstek.Controls.Add(this.btnParaOnayla);
            this.gbParaEklemeIstek.Location = new System.Drawing.Point(12, 225);
            this.gbParaEklemeIstek.Name = "gbParaEklemeIstek";
            this.gbParaEklemeIstek.Size = new System.Drawing.Size(593, 180);
            this.gbParaEklemeIstek.TabIndex = 1;
            this.gbParaEklemeIstek.TabStop = false;
            this.gbParaEklemeIstek.Text = "Para Ekleme İstekleri";
            // 
            // lblPara
            // 
            this.lblPara.AutoSize = true;
            this.lblPara.Location = new System.Drawing.Point(6, 126);
            this.lblPara.Name = "lblPara";
            this.lblPara.Size = new System.Drawing.Size(35, 13);
            this.lblPara.TabIndex = 1;
            this.lblPara.Text = "label1";
            // 
            // btnParaReddet
            // 
            this.btnParaReddet.Location = new System.Drawing.Point(87, 150);
            this.btnParaReddet.Name = "btnParaReddet";
            this.btnParaReddet.Size = new System.Drawing.Size(75, 23);
            this.btnParaReddet.TabIndex = 2;
            this.btnParaReddet.Text = "Reddet";
            this.btnParaReddet.UseVisualStyleBackColor = true;
            this.btnParaReddet.Click += new System.EventHandler(this.btnParaReddet_Click);
            // 
            // dgPara
            // 
            this.dgPara.AllowUserToAddRows = false;
            this.dgPara.AllowUserToDeleteRows = false;
            this.dgPara.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPara.Location = new System.Drawing.Point(6, 19);
            this.dgPara.Name = "dgPara";
            this.dgPara.ReadOnly = true;
            this.dgPara.RowHeadersWidth = 51;
            this.dgPara.Size = new System.Drawing.Size(581, 95);
            this.dgPara.TabIndex = 0;
            this.dgPara.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPara_CellClick);
            this.dgPara.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgPara_CellFormatting);
            // 
            // btnParaOnayla
            // 
            this.btnParaOnayla.Location = new System.Drawing.Point(6, 150);
            this.btnParaOnayla.Name = "btnParaOnayla";
            this.btnParaOnayla.Size = new System.Drawing.Size(75, 23);
            this.btnParaOnayla.TabIndex = 1;
            this.btnParaOnayla.Text = "Onayla";
            this.btnParaOnayla.UseVisualStyleBackColor = true;
            this.btnParaOnayla.Click += new System.EventHandler(this.btnParaOnayla_Click);
            // 
            // btnOturumKapat
            // 
            this.btnOturumKapat.Location = new System.Drawing.Point(491, 415);
            this.btnOturumKapat.Name = "btnOturumKapat";
            this.btnOturumKapat.Size = new System.Drawing.Size(114, 23);
            this.btnOturumKapat.TabIndex = 2;
            this.btnOturumKapat.Text = "Oturumu Kapat";
            this.btnOturumKapat.UseVisualStyleBackColor = true;
            this.btnOturumKapat.Click += new System.EventHandler(this.btnOturumKapat_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 420);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "USD :";
            // 
            // lbl_usd
            // 
            this.lbl_usd.AutoSize = true;
            this.lbl_usd.Location = new System.Drawing.Point(45, 420);
            this.lbl_usd.Name = "lbl_usd";
            this.lbl_usd.Size = new System.Drawing.Size(35, 13);
            this.lbl_usd.TabIndex = 4;
            this.lbl_usd.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(106, 420);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "EUR :";
            // 
            // lbl_eur
            // 
            this.lbl_eur.AutoSize = true;
            this.lbl_eur.Location = new System.Drawing.Point(139, 420);
            this.lbl_eur.Name = "lbl_eur";
            this.lbl_eur.Size = new System.Drawing.Size(35, 13);
            this.lbl_eur.TabIndex = 4;
            this.lbl_eur.Text = "label2";
            // 
            // lbl_gbp
            // 
            this.lbl_gbp.AutoSize = true;
            this.lbl_gbp.Location = new System.Drawing.Point(244, 420);
            this.lbl_gbp.Name = "lbl_gbp";
            this.lbl_gbp.Size = new System.Drawing.Size(35, 13);
            this.lbl_gbp.TabIndex = 4;
            this.lbl_gbp.Text = "label2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(203, 420);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "GBP :";
            // 
            // lblBakiye
            // 
            this.lblBakiye.AutoSize = true;
            this.lblBakiye.Location = new System.Drawing.Point(375, 420);
            this.lblBakiye.Name = "lblBakiye";
            this.lblBakiye.Size = new System.Drawing.Size(35, 13);
            this.lblBakiye.TabIndex = 4;
            this.lblBakiye.Text = "label2";
            // 
            // dataBaseBindingSource1
            // 
            this.dataBaseBindingSource1.DataSource = typeof(yazilimYapimiProje.dataBase.dataBase);
            // 
            // dataBaseBindingSource
            // 
            this.dataBaseBindingSource.DataSource = typeof(yazilimYapimiProje.dataBase.dataBase);
            // 
            // paraIstekBindingSource
            // 
            this.paraIstekBindingSource.DataSource = typeof(yazilimYapimiProje.Entities.paraIstek);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(324, 420);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Bakiye :";
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblBakiye);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_gbp);
            this.Controls.Add(this.lbl_eur);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_usd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOturumKapat);
            this.Controls.Add(this.gbParaEklemeIstek);
            this.Controls.Add(this.gbUrunEklemeIstek);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "admin";
            this.Load += new System.EventHandler(this.frmAdmin_Load);
            this.gbUrunEklemeIstek.ResumeLayout(false);
            this.gbUrunEklemeIstek.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUrun)).EndInit();
            this.gbParaEklemeIstek.ResumeLayout(false);
            this.gbParaEklemeIstek.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPara)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paraIstekBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbUrunEklemeIstek;
        private System.Windows.Forms.Button btnUrunReddet;
        private System.Windows.Forms.Button btnUrunOnayla;
        private System.Windows.Forms.DataGridView dgUrun;
        private System.Windows.Forms.GroupBox gbParaEklemeIstek;
        private System.Windows.Forms.Button btnParaReddet;
        private System.Windows.Forms.DataGridView dgPara;
        private System.Windows.Forms.Button btnParaOnayla;
        private System.Windows.Forms.Button btnOturumKapat;
        private System.Windows.Forms.BindingSource dataBaseBindingSource;
        private System.Windows.Forms.BindingSource paraIstekBindingSource;
        private System.Windows.Forms.BindingSource dataBaseBindingSource1;
        private System.Windows.Forms.Label lblUrun;
        private System.Windows.Forms.Label lblPara;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_usd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_eur;
        private System.Windows.Forms.Label lbl_gbp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblBakiye;
        private System.Windows.Forms.Label label2;
    }
}