﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using yazilimYapimiProje.Entities;


namespace yazilimYapimiProje
{
    public partial class urunEkle : Form
    {
        public urunEkle()
        {
            InitializeComponent();
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            var ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(pbUnlem, "Lütfen miktarı kg cinsinde giriniz");
        }

        urunler u;
        private void btnEkle_Click(object sender, EventArgs e)
        {
            #region Ekleme islemi

            //Açık olan formlardan adı urunEkle olan formu yakalıyor.
            var urunFormu = Application.OpenForms["urunEkle"];
            //urunFormunu içindeki group boxu yakalıyor.
            if (urunFormu != null)
            {
                var gbTextBox = (GroupBox)urunFormu.Controls["gbUrunTxt"];

                var textBoxKontrol = true;
                //Groupboxın içindeki kontrollerde geziyor.
                foreach (Control item in gbTextBox.Controls)
                    //Eğer texbox ya da combobox sa buraya giriyor.
                    if (item is TextBox || item is ComboBox)
                        //Eğer itemin içindeki değer boş ise hata verip textBoxKontrolü false yapıyor.
                        if (string.IsNullOrEmpty(item.Text))
                        {
                            MessageBox.Show("Lütfen tüm alanları doldurun");
                            textBoxKontrol = false;
                            break;
                        }

                //Eğer Texoxlar boş değilse vce texboxların içindeki değer sayı ise buaraya giriyor.
                if (textBoxKontrol && textBoxSayiControl())
                {
                    u = new urunler(cmbUrun.SelectedItem.ToString(), int.Parse(txtMiktar.Text),
                        int.Parse(txtFiyat.Text));
                    //Eğer propertieslerden gelen eklemeGeçerlilik true ise buraya giriyor.
                    if (u.eklemeGecerlilik) urunler.urunEkle(u); //Urun ekleme işlemini yapan fonksiyonu çalıştırıyor.
                }
            }

            
            #endregion
        }

        //Miktar ve fiyat texboxuna gelen değer sayı mı diye kontrol ediyor.
        private bool textBoxSayiControl()
        {
            // txtFiyat texboxunun içindeki değer sayı mı diye bakıyor ve sayı ise txtKontrol değişkenine true yazıyor.
            var txtKontrol = txtFiyat.Text.All(char.IsDigit);
            //Eğer sayı ise buraya giriyor.
            if (txtKontrol)
                //txtMiktar texboxu sayı mı diye bakıyor ve sayı ise txtKontrol değişkenine true yazıyor.
                txtKontrol = txtMiktar.Text.All(char.IsDigit);
            //Eğer sayı değilse hata veriyor.
            else
                MessageBox.Show("Lütfen sayısal değer giriniz");

            return txtKontrol;
        }

        
    }
}